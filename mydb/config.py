import os

class Config:
    # Database config
    def __init__(self):
        self.db_name = os.getenv('DATABASE_NAME')
        self.db_port = os.getenv('DATABASE_PORT')
        self.db_user = os.getenv('DATABASE_USERNAME')
        self.db_password = os.getenv('DATABASE_PASSWORD')
        self.db_host = os.getenv('DATABASE_HOST')
