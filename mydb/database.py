import logging as logger
import pymysql
from configparser import ConfigParser
from flask import jsonify

query_parser = ConfigParser()
query_parser.read('./mydb/queries.config')

class Database:
    INSERT = 'INSERT'
    UPDATE = 'UPDATE'
    DELETE = 'DELETE'
    SELECT = 'SELECT'

    def __init__(self, config):
        self.dbname = config.db_name
        self.port = config.db_port
        self.host = config.db_host
        self.username = config.db_user
        self.password = config.db_password
        self.conn = None

    def open_connection(self):
        """Connect to MySQL Database"""
        try:
            if self.conn is None:
                self.conn = pymysql.connect(host=self.host,
                                            user=self.username,
                                            password=self.password,
                                            db=self.dbname,
                                            charset='utf8mb4',
                                            connect_timeout=15,
                                            cursorclass=pymysql.cursors.DictCursor)
        except pymysql.MySQLError as e:
            logger.error(e)
        finally:
            logger.info('Connection opened successfully.')

    def insert(self, query):
        """Execute MySQL query"""
        try:
            self.open_connection()
            with self.conn.cursor() as cur:
                logger.debug("Query----> {}".format(query))
                cur.execute(query)
                self.conn.commit()
                affected = cur.rowcount
                cur.close()
                return affected
        except pymysql.MySQLError as e:
            logger.error(e)
            return 0
        finally:
            if self.conn:
                self.conn.close()
                self.conn = None
                logger.info('Database connection closed.')

    def _gen_insert_query(self, table_name, data):
        qry = "INSERT INTO " + table_name + " ({0}) VALUES ({1})"
        cols = ''
        values = ''
        for key in data:
            cols += "{},".format(key)
            values += "'{}',".format(data[key])
        return qry.format(cols[:-1], values[:-1])


    def _gen_insert_query_exclude_cols(self, table_name, columns, data):
        qry = "INSERT INTO " + table_name + " ({0}) VALUES ({1})"
        cols = ''
        values = ''
        for key in columns:
            if key in data and data[key] != "None" and data[key] != "null" and data[key] != "NULL" and data[
                key] != "undefined":
                cols += "{},".format(key)
                values += "'{}',".format(data[key])
        return qry.format(cols[:-1], values[:-1])


    def _gen_update_query(self, table_name, columns, data):
        qry = "UPDATE " + table_name + " SET"
        update = ''
        for key in columns:
            if key in data and data[key] != "None" and data[key] != "null" and data[key] != "NULL" and data[
                key] != "undefined":
                update += " {}=\"{}\",".format(key, data[key])
        if not len(update):
            return None
        return qry + update[:-1]

    def _gen_upsert_query(self, table_name, columns, data):
        qry = "INSERT INTO " + table_name + " ({0}) VALUES ({1}) ON DUPLICATE KEY UPDATE {2}"
        update = ''
        cols = ''
        values = ''
        for key in columns:
            if key in data and data[key] != "None" and data[key] != "null" and data[key] != "NULL" and data[
                key] != "undefined":
                cols += "{},".format(key)
                values += "\"{}\",".format(data[key])
                update += " {}=\"{}\",".format(key, data[key])
        if not len(update):
            return None
        return qry.format(cols[:-1], values[:-1], update[:-1])


    def _get_columns(self, name):
        _schema = self.run_query("SELECT", query_parser.get('general', '_get_table_schema').format(name))
        col_list = list()
        for col in _schema:
            col_list.append(col.get('COLUMN_NAME'))
        return col_list


    def _bulk_insert(self, query, records):
        """Execute bulk insert query"""
        try:
            self.open_connection()
            with self.conn.cursor() as cur:
                logger.debug("Query----> {}".format(query))
                cur.executemany(query, records)
                self.conn.commit()
                cur.close()
                return True
        except pymysql.MySQLError as e:
            logger.error(e)
            return 0
        finally:
            if self.conn:
                self.conn.close()
                self.conn = None
                logger.info('Database connection closed.')


    def run_query(self, query_type, query, row_insert_id=False):
        """Execute MySQL query"""
        try:
            self.open_connection()
            with self.conn.cursor() as cur:
                if query_type is not None and (query_type.upper() in (self.INSERT, self.UPDATE, self.DELETE)):
                    logger.debug("Query----> {}".format(query))
                    cur.execute(query)
                    self.conn.commit()
                    affected = cur.rowcount
                    if row_insert_id:
                        last_row_id = cur.lastrowid
                        cur.close()
                        return affected, last_row_id
                    cur.close()
                    return affected
                elif query_type is not None or query_type.upper() == self.SELECT:
                    logger.debug("Query----> {}".format(query))
                    cur.execute(query)
                    result = cur.fetchall()
                    cur.close()
                    return result
                else:
                    return None
        except pymysql.MySQLError as e:
            logger.error(e)
            return 0
        finally:
            if self.conn:
                self.conn.close()
                self.conn = None
                logger.info('Database connection closed.')