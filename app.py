from flask import Flask
from flask_cors import CORS

# set the project root directory as the static folder, you can set others.
app = Flask(__name__, static_url_path='/var/www/petromall')
CORS(app, supports_credentials=True)
# cors = CORS(app, resources={r"/*": {"origins": "*"}})
