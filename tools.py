from app import app
from flask import request, jsonify

import numpy as np
import logging as logger

@app.route('/emi/solar', methods=['GET'])
def _solar_emi():
    _json = request.json
    _roi = _json['roi']
    _years = _json['years']
    _amount = _json['amount']
    _emi_amount = round(np.pmt(_roi/(12*100), _years * 12, -(_amount)))
    return jsonify({'name':'Solar EMI', 'amount': _emi_amount})

@app.route('/emi/fuel', methods=['GET'])
def _fuel_emi():
    _json = request.json
    _roi = _json['roi']
    _days = _json['days']
    _amount = _json['amount']
    _emi_amount = round(_amount+(_amount*_roi/100)*(_days/360))
    return jsonify({'name':'Fuel EMI', 'amount': _emi_amount})

@app.route('/processfee', methods=['GET'])
def _processing_fee():
    _json = request.json
    _amount = _json['amount']
    _proc_fee = _json['fee']
    _gst = _json['gst']
    _proc_fee = (_amount*_proc_fee/100)
    return jsonify({'name': 'Processing Fee', 'amount': round(_proc_fee*(1+(_gst/100)))})

@app.errorhandler(404)
def not_found(error=None):
    message = {
        'status': 404,
        'message': 'Not Found: ' + request.url,
    }
    resp = jsonify(message)
    resp.status_code = 404

    return resp

# Run Tools Server
if __name__ == '__main__':
    logger.debug('Starting the tools application', )
    app.run(host="localhost", port="5100", debug="True")

