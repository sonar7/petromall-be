import boto3
import binascii
import datetime
import time
import hashlib
import logging as logger
import requests as req
import os
import xmltodict
import json
from collections import Counter
from functools import reduce
from itertools import groupby
from operator import itemgetter, add
from configparser import ConfigParser
import razorpay
import pandas as pd
import itertools
import cx_Oracle
import pdfkit
from dotenv import load_dotenv
from flask import jsonify, request, redirect, send_from_directory
from flask_jwt_extended import (
    JWTManager, jwt_refresh_token_required,
    create_access_token, create_refresh_token, get_jwt_identity, jwt_required
)
from flask_mail import Mail, Message
from werkzeug.utils import secure_filename
import decimal
import base64
import uuid

from app import app
from mydb import Database, dbConfig

query_parser = ConfigParser()
error_parser = ConfigParser()
query_parser.read('./mydb/queries.config')
error_parser.read('./mydb/error.config')

# load environment variables
load_dotenv()

mail_config = {
    "MAIL_SERVER": 'smtp.gmail.com',
    "MAIL_PORT": 465,
    "MAIL_USE_TLS": False,
    "MAIL_USE_SSL": True,
    "MAIL_USERNAME": os.getenv('EMAIL_USER'),
    "MAIL_PASSWORD": os.getenv('EMAIL_PASSWORD')
}

_wkhtml_config = pdfkit.configuration(wkhtmltopdf=os.getenv('WKHTML_CONFIG'))
report_recipients = os.getenv('LOAN_REPORT_EMAIL_LIST').split(",")
recipients = os.getenv('LOAN_APPLICATION_EMAIL_LIST').split(",")
alert_recipients = os.getenv('SYSTEM_ALERT_EMAIL_LIST').split(",")
transporter_docs_bucket = os.getenv('TRANSPORTER_DOCS_BUCKET')
dealership_docs_bucket = os.getenv('DEALERSHIP_DOCS_BUCKET')
docs_url = os.getenv('DOCS_URL')

ALLOWED_EXTENSIONS = {'pdf', 'png', 'jpg', 'jpeg', 'xls', 'xlsx', 'csv'}
SMS_API_AUTH_KEY = '293004Af9qkN64BTm85d736ebe'
OTP_TEMPLATE = '5e60da24d6fc0535104160ac'
LEEGALITY_URL = os.getenv('LEEGALITY_URL')
LEEGALITY_AUTH_TOKEN = os.getenv('LEEGALITY_AUTH_TOKEN')
LEEGALITY_WEB_HOOK = os.getenv('LEEGALITY_WEB_HOOK')
RPAY_CLIENT_KEY = 'rzp_test_a82maRLnc9LvMw'
RPAY_SECRET_KEY = 'QQEbYgot6rdM3cvC1IEtMvJz'
BBPS_UAT_TENANT_ID = 'ID01'
BBPS_UAT_API_URL = 'https://uatbbps.idbibank.com/idbibbps/APIService/'
BBPS_UAT_USERNAME = "ID01AI09519210660159"
BBPS_UAT_PASSWORD = "password"
BBPS_AUTH = req.auth.HTTPBasicAuth(BBPS_UAT_USERNAME, BBPS_UAT_PASSWORD)

options = {
    'page-size': 'A4',
    'margin-top': '0.3in',
    'margin-right': '0.5in',
    'margin-bottom': '1.5in',
    'margin-left': '0.5in',
    'encoding': "UTF-8",
    'footer-html': 'template/footer.html',
    'custom-header': [
        ('Accept-Encoding', 'gzip')
    ],
    'cookie': [
        ('cookie-name1', 'cookie-value1'),
        ('cookie-name2', 'cookie-value2'),
    ],
    'no-outline': None,
    'enable-local-file-access': None
}

# database is using values from .env file this import should be after the load env
logger.basicConfig(level='DEBUG')

# database object
mydb = Database(dbConfig())

# Razorpay client
_rpay_client = razorpay.Client(auth=(RPAY_CLIENT_KEY, RPAY_SECRET_KEY))

app.config['PROJECT_DIR'] = os.getcwd()
app.config['UPLOAD_FOLDER'] = "data"
app.config['TEMPLATE_DIR'] = app.config['PROJECT_DIR'] + "/template"
app.config['APPLICATION_TEMPLATE'] = app.config['TEMPLATE_DIR'] + "/application.html"
app.config['SANCTION_LETTER_TEMPLATE'] = app.config['TEMPLATE_DIR'] + "/sanction_letter.html"
app.config['EXPERIAN_TEMPLATE'] = app.config['TEMPLATE_DIR'] + "/experian-cons-req-template.xml"
app.config['TARGET_FILE_PATH'] = app.config['PROJECT_DIR'] + "/" + app.config['UPLOAD_FOLDER']
app.config['MAX_CONTENT_LENGTH'] = 50 * 1024 * 1024

# app.json_encoder = MyJSONEncoder
logger.debug("Project directory {}".format(app.config['PROJECT_DIR']))
logger.debug("Templates directory {}".format(app.config['TEMPLATE_DIR']))
logger.debug("Application template file {}".format(app.config['APPLICATION_TEMPLATE']))
logger.debug("Sanction letter file {}".format(app.config['SANCTION_LETTER_TEMPLATE']))
logger.debug("Loan report email list:{}".format(report_recipients))
logger.debug("Application email list:{}".format(recipients))

# Configure application to store JWTs in cookies. Whenever you make
# a request to a protected endpoint, you will need to send in the
# access or refresh JWT via a cookie.
# app.config['JWT_TOKEN_LOCATION'] = ['cookies']

# Only allow JWT cookies to be sent over https.
# In production, this should likely be True
# app.config['JWT_COOKIE_SECURE'] = False

# Set the cookie paths, so that you are only sending your access token
# cookie to the access endpoints, and only sending your refresh token
# to the refresh endpoint. Technically this is optional, but it is in
# your best interest to not send additional cookies in the request if
# they aren't needed.
app.config['JWT_ACCESS_COOKIE_PATH'] = '/api/'
app.config['JWT_REFRESH_COOKIE_PATH'] = '/login/check'

# Enable csrf double submit protection. See this for a thorough
# explanation: http://www.redotheweb.com/2015/11/09/api-security.html
# app.config['JWT_COOKIE_CSRF_PROTECT'] = False

# JWT secret - Set the secret key to sign the JWTs with
app.config['JWT_SECRET_KEY'] = os.getenv('JWT_SECRET')

# s3 Role ARN
app.config['S3_ROLE_ARN'] = os.getenv('ROLE_ARN')

jwt = JWTManager(app)


# Basic API route
@app.route('/', methods=['GET'])
@app.route('/api', methods=['GET'])
def main():
    return redirect("https://www.petromoney.in", code=302)


@app.route('/data/<path:path>')
def send_data_file(path):
    return send_from_directory('data', path)


@app.route('/template/<path:path>')
def send_js(path):
    return send_from_directory('template', path)


@app.route('/app/version', methods=['GET'])
def _app_version():
    _version = mydb.run_query(mydb.SELECT, query_parser.get('app', '_get_app_version'))
    logger.debug("Current app version: {}".format(_version))
    return jsonify({"status": "SUCCESS", "version": _version[0].get('version')})


@app.route('/api/user/agreement/<string:device_id>', methods=['GET'])
@app.route('/api/user/agreement', methods=['POST'])
def _user_agreement(device_id=None):
    if device_id:
        result = _get_result_as_dict(query_parser.get('app', '_get_user_agreement').format(device_id))
        if result:
            return jsonify({"status": "SUCCESS", "message": "User agreed the Terms.", "data": result})
        else:
            return jsonify({"status": "ERROR", "message": "Please, agree to the Terms to continue."})
    if request.method == 'POST':
        data = request.get_json()
        logger.debug("Agreement info: {}".format(data))
        logger.debug("Status info: {}".format(data.get('status')))
        _agreement_data = {"device_id": data.get('device_id'),
                            "details": json.dumps(data.get("details"))
                            }
        _query = mydb._gen_insert_query('t_user_agreement', _agreement_data)
        return _execute_query(mydb.INSERT, _query)


@app.route('/api/bot/delivery/report', methods=['POST'])
def _whatsapp_msg_report():
    data = request.get_json()
    _query = mydb._gen_insert_query('whatsapp_report', data)
    mydb.run_query(mydb.INSERT, _query)
    return jsonify(["success"])


def fuel_credit_due(mobile):
    _id = mydb.run_query(mydb.SELECT, query_parser.get('dealers', '_get_dealership_id').format(mobile))
    if not _id:
        return "No dealership found with this mobile number"
    logger.debug("Fuel credit outstandings of dealership id: {}".format(_id[0].get('dealership_id')))
    _data = _get_loan_due_report(_id[0].get('dealership_id')).get_json()
    # logger.debug("D/OD report:")
    # logger.debug(_data)
    _dues = ""
    _over_dues = ""
    if _data.get("status") == "SUCCESS":
        if _data.get('data').get('due'):
            _dues = "*List of Due(s):*\n "
            for due in _data.get('data').get('due'):
                _dues = _dues + "Disbursement date : {0}\n " \
                                "Disbursement amount : {1}\n " \
                                "Due date : {2}\n " \
                                "*Principal due : {3}*\n " \
                                "*Interest due : {4}*\n " \
                                "*Total due : {5}*\n\n " \
                    .format(
                    due.get("disb_date"),
                    due.get("disb_amt"),
                    due.get("duedate"),
                    due.get("prin_due"),
                    due.get("int_due"),
                    due.get("tot_due"))
        else:
            _dues = "No dues found.\n "

        if _data.get('data').get('overdue'):
            _over_dues = "*List of Overdue(s):*\n "
            for overdue in _data.get('data').get('overdue'):
                _over_dues = _over_dues + "Disbursement date : {0}\n " \
                                "Disbursement amount : {1}\n " \
                                "Due date : {2}\n " \
                                "*Principal Overdue : {3}*\n " \
                                "*Interest Overdue : {4}*\n " \
                                "*Penal Overdue : {5}*\n " \
                                "*Total Due : {6}* \n " \
                                "*Days past Due (DPD): {7}*\n\n "\
                    .format(
                    overdue.get("disb_date"),
                    overdue.get("disb_amt"),
                    overdue.get("duedate"),
                    overdue.get("prin_overdue"),
                    overdue.get("int_overdue"),
                    overdue.get("penal_overdue"),
                    overdue.get("tot_due"),
                    overdue.get("dpd"))
        else:
            _over_dues = "No overdue found.\n "
        return _dues + "\n" + _over_dues + "\n"


def coming_soon():
    return "coming soon"


def find_trail(q_no, res, mobile):
    q_no_map = {
        "1": {
            "1": (1, fuel_credit_due(mobile[2:])),
            "2": (1, coming_soon()),
            "3": (1, coming_soon())  # 2
        },
        "2": {
            "2": (1, coming_soon())  # 3
        },
        "3": {
            "1": (1, coming_soon()),  # 4
            "2": (1, coming_soon()),  # 4
            "3": (1, coming_soon()),  # 4
            "4": (1, coming_soon())  # 4
        },
        "4": {
            "1": (1, coming_soon()),  # 1
            "2": (1, coming_soon())  # 1
        }
    }
    if int(q_no) > 1:
        res = 1
    return q_no_map.get(str(q_no)).get(str(res))


@app.route('/api/bot', methods=['POST'])
def _whatsapp_bot(_trail=0, _pre=""):
    logger.debug("Whatsapp bot request input data: {}".format(request.get_json()))
    data = request.get_json().get("message")
    _msg_ts = time.strftime('%Y-%m-%d %H:%M:%S', time.localtime(data.get('timestamp')))
    data.update({"from_ph": data.get('from'), "to_ph": data.get('to'), "msg_ts": _msg_ts})
    _cols = mydb._get_columns('whatsapp_conversation')
    if data.get('text').get('body').lower() == 'hi':
        if not _dealer_exists(data.get('from')[2:]):
            return "Dealer is not registered with Petromoney. Contact, hello@petromoney.in"
        data.update({"q_id":1})
        _query = mydb._gen_insert_query_exclude_cols('whatsapp_conversation', _cols, data)
        mydb.run_query(mydb.INSERT, _query)
        _q_a = mydb.run_query(mydb.SELECT, "SELECT question FROM whatsapp_q_a WHERE id=1")
        # logger.debug("Whatsapp API response: {}".format(_q_a[0].get('question')))
        # logger.debug(type(_q_a[0].get('question')))
        return "Hi, I am Petroman. How can I help you?\n" \
               "1. Outstanding Fuel Credit\n" \
               "2. Request Loan reload\n" \
               "3. Statement of Accounts\n" \
               "4. Pay convenience fee\n" \
               "5. Request callback\n"
    if data.get('text').get('body'):
        _prev = mydb.run_query(mydb.SELECT,
                               "SELECT id, from_ph, to_ph, q_id, msg_ts, ans FROM whatsapp_conversation WHERE from_ph='{}' ORDER BY msg_ts DESC LIMIT 1".format(
                                   data.get('from')))
        logger.debug("Previous data:{}".format(_prev))
        if _prev:
            _up = mydb.run_query(mydb.UPDATE,
                                 "UPDATE whatsapp_conversation SET ans='{0}' WHERE id='{1}'".format(
                                     data.get('text').get('body'), _prev[0].get('id'))
                                 )
            _trail, _pre = find_trail(_prev[0].get('q_id'), data.get('text').get('body'), mobile=data.get('from'))
            # logger.debug("Trail q_id: {}".format(_trail))
            # logger.debug("Response to question: {}".format(_pre))
            if not _trail:
                return "Invalid input, Say \'Hi\' to start conversation!"
            return _pre
        else:
            return "Invalid input, Say \'Hi\' to start conversation!"
    else:
        return "Invalid message input, type Hi to initiate the conversation."


# Login route
# Use the set_access_cookie() and set_refresh_cookie() on a response. By default, the CRSF cookies will be called csrf_access_token and
# csrf_refresh_token, and in protected endpoints we will look for the CSRF token in the 'X-CSRF-TOKEN' header. You can modify all of these
# with various app.config options. Check the options page for details. JWT_COOKIE_CSRF_PROTECT set to True, set_access_cookies() and
# set_refresh_cookies() will now also set the non-httponly CSRF cookies as well
@app.route('/api/login/otp', methods=['POST'])
def login():
    mobile = request.json.get('mobile', None)
    if mobile is not None:
        _get_user = mydb.run_query(mydb.SELECT, query_parser.get('users', '_get_user_info').format(mobile))
        if len(_get_user):
            if _get_user[0].get('status') == 0:
                return jsonify({'status': 'ERROR', 'message': 'Unauthorized access, user does not exist.'})
            send_otp(_get_user[0].get('mobile'), OTP_TEMPLATE, 2)
            return jsonify(
                {'status': 'SUCCESS', 'mobile': _get_user[0].get('mobile'), 'role_id': _get_user[0].get('role_id')})
        else:
            return jsonify({'status': 'ERROR', 'message': 'User does not exist.'})
    else:
        return jsonify({'status': 'ERROR', 'message': 'Missing mobile number.'})


@app.route('/api/login/user', methods=['POST'])
def login_user_otp():
    mobile = request.json.get('mobile')
    if mobile:
        _get_user = mydb.run_query(mydb.SELECT, query_parser.get('users', '_get_user_info').format(mobile))
        if len(_get_user):
            if _get_user[0].get('status') == 0:
                return jsonify({'status': 'ERROR', 'message': 'Unauthorized access, user does not exist.'})
    password = request.json.get('password')
    otp = request.json.get('otp')
    if otp:
        response = verify_otp(mobile, otp)
        if response.get('type') == 'success':
            return _get_user_data(mobile, password)
        else:
            return jsonify({'status': 'ERROR', 'message': response.get('message')})
    elif password is not None:
        return _get_user_data(mobile, password)
    else:
        return jsonify({'status': 'ERROR', 'message': 'Login requires Password or OTP.'})


def _get_user_data(mobile, password):
    result = mydb.run_query(mydb.SELECT, query_parser.get('users', '_get_user_info').format(mobile))
    data = dict()
    if (len(result) != 0):
        result = result[0]
        logger.debug(result)
        if result.get('status') == 0:
            return jsonify({'status': 'ERROR', 'message': 'Unauthorized access, user does not exist.'})
        if password is not None:
            if not _verify_password(result.get('password'), password):
                return jsonify({'status': 'ERROR', 'message': 'Incorrect username or password'})
        logger.debug("User data result: {}".format(result))
        data.update({'id': result.get('id'), 'first_name': result.get('first_name'), 'last_name': result.get('last_name'),
                     'mobile': result.get('mobile'),
                     'email': result.get('email'), 'role_id': result.get('role_id'),
                     'role_name': result.get('role_name'), 'role_desc': result.get('role_desc')})
        data.update({"regions_mapped": _get_user_regions(result.get('id'))})
        _get_regions = mydb.run_query(mydb.SELECT,
                                      query_parser.get('users', '_get_user_region_info').format(result.get('id')))
        if len(_get_regions):
            data.update(
                {'region_id': _get_regions[0].get('region_id'),
                 'region_name': _get_regions[0].get('region_name')})
        if (data.get('role_id') == 13):
            response = mydb.run_query(mydb.SELECT, query_parser.get('dealers', '_get_dealership_id').format(mobile))
            if len(response):
                data.update({'dealership_id': response[0].get('dealership_id')})
            else:
                return jsonify(
                    {'status': 'ERROR', 'message': 'Unable to Login, no dealers found with this mobile number.'})
        if (data.get('role_id') == 14):
            response = mydb.run_query(mydb.SELECT, query_parser.get('transporters', '_get_transport_owner_id').format(mobile))
            if len(response):
                data.update({'transporters': response})
        # Create the tokens we will be sending back to the user
        expires = datetime.timedelta(days=1)
        access_token = create_access_token(identity=data.get('id'), expires_delta=expires)
        refresh_token = create_refresh_token(identity=data.get('id'))
        data.update({'token': access_token, 'refresh_token': refresh_token})
        # Set the JWT cookies in the response
        return jsonify({'status': 'SUCCESS', 'message': 'Login success', 'data': data})
    else:
        return jsonify({'status': 'ERROR', 'message': 'User does not exists.'})


def track_user_login(login_data):
    _query = mydb._gen_insert_query('t_login_history', login_data)
    affected_rows = mydb.run_query(mydb.INSERT, _query)
    return True if affected_rows > 0 else False


@app.route('/api/users', methods=['GET'])
@jwt_required
def _get_all_users():
    current_user = get_jwt_identity()
    user_role = mydb.run_query(mydb.SELECT, query_parser.get('users', '_get_user_role').format(current_user))
    _allowed_roles = [1, 3, 6, 8, 9]
    if request.method == 'GET':
        if user_role[0].get('role_id') in _allowed_roles:
            result = _get_result_as_dict(query_parser.get('users', '_get_user_details'))
            return jsonify({'status': 'SUCCESS', 'data': result})
        else:
            return jsonify({'status': 'ERROR', 'message': error_parser.get('invalid', '_unauthorized')})


# Because the JWTs are stored in an httpOnly cookie now, we cannot log the user out by simply deleting the cookie
# in the frontend. We need the backend to send us a response to delete the cookies in order to logout.
# unset_jwt_cookies is a helper function to do just that.
@app.route('/api/logout', methods=['POST'])
def logout():
    return jsonify({'status': 'SUCCESS', 'logout': True})


@app.route('/api/login/check', methods=['POST'])
@jwt_refresh_token_required
def login_check():
    # Create the new access token
    current_user = get_jwt_identity()
    expires = datetime.timedelta(days=1)
    access_token = create_access_token(identity=current_user, expires_delta=expires)
    # Set the access JWT and CSRF double submit protection cookies in this response
    logger.debug('The JWT auth token has been refreshed.')
    return jsonify({'status': 'SUCCESS', 'data': access_token, 'refresh': True})


@app.route('/api/signup', methods=['POST'])
def _signup_user():
    data = request.get_json()
    role_id = data.get('role_id', 12)  # default sales person role_id
    email = data.get('email', '')
    # default password setting
    password = data.get('password', 'Petromall@2020')
    data.update({'role_id': role_id, 'email': email, 'password': password})
    logger.debug(data)
    return _signup(data)


@app.route('/api/user/<int:user_id>', methods=['POST'])
@jwt_required
def _update_user_details(user_id=None):
    if user_id is not None:
        logger.debug("User details update request for ID: {0} by login ID:{0}".format(user_id, get_jwt_identity()))
        data = request.get_json()
        data.update({"last_modified_by": get_jwt_identity()})
        _user_cols = mydb._get_columns('m_users')
    if "password" in data:
        data.update({'password': _hash_password(data.get('password'))})
    if _user_exists(user_id):
        logger.debug("Users details to be updated are: {}".format(data))
        _query = mydb._gen_update_query('m_users', _user_cols, data)
        return _execute_query(mydb.UPDATE, _query + " WHERE id={}".format(user_id))
    else:
        return jsonify(
            {'status': 'ERROR', 'message': 'No user found with the details. Please contact your administrator.'})


@app.route('/api/user/<int:user_id>', methods=['DELETE'])
@jwt_required
def _disable_user(user_id=None):
    if request.method == 'DELETE':
        if user_id is not None:
            mydb.run_query(mydb.UPDATE, "UPDATE m_users SET status=0, last_modified_by={0} WHERE id={1}".format(get_jwt_identity(),user_id))
            return jsonify({"status": "SUCCESS", "message": "The user has been disabled."})


@app.route('/api/signup/dealer', methods=['POST'])
def _signup_dealer():
    data = request.get_json()
    id = data.get('dealership_id')
    name = data.get('dealership_name')
    role_id = data.get('role_id', 13)  # default dealer role_id
    email = data.get('email', '')
    # default password setting
    password = data.update({'password': data.get('password', 'Petromall@2020')})
    _signup_response = _signup(data)
    logger.debug("Signup response: {}".format(_signup_response.get_json()))
    data.update({'id': id, 'name': name, 'role_id': role_id, 'email': email, 'password': password})
    if _signup_response.get_json().get('status') == 'SUCCESS':
        dealership_cols = ["id", "name", "business_type", "pincode", "omc", "region"]
        dealer_cols = ["dealership_id", "first_name", "last_name", "email", "mobile"]
        try:
            logger.info("user data in signup request for dealership registration: {}".format(data))
            _insert_dealership = mydb._gen_upsert_query("m_dealership", dealership_cols, data)
            _execute_query(mydb.INSERT, _insert_dealership)
            _insert_dealer = mydb._gen_upsert_query("m_dealers", dealer_cols, data)
            _execute_query(mydb.INSERT, _insert_dealer)
        except Exception as e:
            logger.error("Dealership/Dealer details can not be updated due to an error: {}".format(e))
            return _signup_response
    return _signup_response


@app.route('/api/signup/transporter', methods=['POST'])
def _signup_transporter():
    data = request.get_json()
    logger.debug("Transport signup details on request: {}".format(data))
    role_id = data.get('role_id', 14)  # default dealer role_id
    email = data.get('email', '')
    # default password setting
    password = data.get('password', 'Petromall@2020')
    data.update({'role_id': role_id, 'email': email, 'password': password})
    return _signup(data)


def _signup(data, otp=True):
    if (not data.get('first_name')) or (not data.get('email')) or (not data.get('mobile')):
        return jsonify({'status': 'ERROR', 'message': 'Missing user details (First name, Email or Mobile)'})
    _user_exists = mydb.run_query(mydb.SELECT, query_parser.get('users', '_get_user_info').format(data.get('mobile')))
    if _user_exists:
        logger.debug("User exists: {}".format(_user_exists))
        return jsonify({'status': 'ERROR', 'message': 'User with this mobile number exists, log in to continue.'})
    data.update({'password':_hash_password(data.get('password', 'Petromall@2020'))})
    _user_cols = mydb._get_columns('m_users')
    _user_query = mydb._gen_insert_query_exclude_cols('m_users', _user_cols, data)
    affected_rows, pm_user_id = mydb.run_query(mydb.INSERT, _user_query, True)
    if not affected_rows:
        return jsonify({'status': 'ERROR', 'message': 'Failed to add new user account.'})
    logger.debug("New user account is created with user_id:{}".format(pm_user_id))
    if affected_rows > 0 and otp:
        _user_otp_response = send_otp(data.get('mobile'), OTP_TEMPLATE, 2)
        logger.debug("user otp response sent: {}".format(_user_otp_response))
    if data.get('role_id') == 14:
        data.update({'t_owner_id':pm_user_id})
        _cols = mydb._get_columns('t_transport_owners')
        _query = mydb._gen_insert_query_exclude_cols('t_transport_owners', _cols, data)
        affected_rows = mydb.run_query(mydb.INSERT, _query)
        if not affected_rows:
            logger.debug('Failed to add new Transport owner account.')
    return jsonify({'status': 'SUCCESS', 'message': 'Welcome to Petromoney. Please, login to access your user account.'})


# Protected route
@app.route('/api/user/roles', methods=['GET', 'POST'])
def user_roles():
    if request.method == 'POST':
        data = request.get_json()
        qry = "INSERT INTO m_roles(role_name, role_desc) values('{0}', '{1}')" \
              " ON DUPLICATE KEY UPDATE role_desc='{1}'" \
            .format(data.get('name'), data.get('desc'))
        affected_rows = mydb.run_query(mydb.INSERT, qry)
        if affected_rows:
            return jsonify({'status': 'SUCCESS'})
        else:
            return jsonify({'status': 'ERROR', 'message': 'Failed to add user role'})
    elif request.method == 'GET':
        result = mydb.run_query(mydb.SELECT, 'SELECT id, role_name, role_desc as name from m_roles')
        return jsonify({'status': 'SUCCESS', 'data': result})


@app.route('/api/banks', methods=['GET', 'POST'])
@jwt_required
def banks():
    if request.method == 'GET':
        return _get_result(query_parser.get('general', '_get_banks'))
    elif request.method == 'POST':
        data = request.get_json()
        return _is_rows_affected(query_parser.get('general', '_post_banks').format(data.get('name')))


@app.route('/api/omcs', methods=['GET', 'POST'])
def omcs():
    if request.method == 'GET':
        result = _get_result(query_parser.get('general', '_get_omcs'))
        data = result.get_json().get('data') + [{'id': 'NA', 'name': 'Others'}]
        return jsonify({'status': 'SUCCESS', 'data': data})
    elif request.method == 'POST':
        data = request.get_json()
        return _is_rows_affected(query_parser.get('general', '_post_omcs').format(data.get('name')))


@app.route('/api/vehicle/loan/options', methods=['GET', 'POST'])
@app.route('/api/vehicle/loan/options/<int:id>', methods=['POST'])
def transporter_loan_options():
    if request.method == 'POST':
        data = request.get_json()
        if id is not None:
            qry = "INSERT INTO m_transporter_loan_options(credit_head, credit_desc, is_service) values('{0}', '{1}')" \
                  " ON DUPLICATE KEY UPDATE credit_desc='{1}'" \
                .format(data.get('credit_head'), data.get('credit_desc'), data.get('is_service', 0))
            return _is_rows_affected(qry)
    elif request.method == 'GET':
        result = mydb.run_query(mydb.SELECT,
                                'SELECT id, credit_head, credit_desc, is_service from m_transporter_loan_options')
        return jsonify({'status': 'SUCCESS', 'data': result})


@app.route('/api/dealership', methods=['GET'])
@app.route('/api/dealership/<int:id>', methods=['GET', 'POST', 'DELETE'])
def dealership(id=None):
    if request.method == 'POST':
        data = dict(request.form)
        logger.debug("Dealership data: %s", data)
        if request.files is not None:
            try:
                for file in request.files:
                    logger.debug("File in request object : {}".format(file))
                    if (file == 'pan_file_url'):
                        _status, pan_file_url = _file_upload(request.files.getlist(file), id, doc_id=2)
                        logger.debug("pan file url: %s", pan_file_url)
                        data.update({'pan_file_url': pan_file_url})
                    if (file == 'gst_file_url'):
                        _status, gst_file_url = _file_upload(request.files.getlist(file), id, doc_id=10)
                        logger.debug("gst file url: %s", gst_file_url)
                        data.update({'gst_file_url': gst_file_url})
            except OSError as e:
                logger.debug('Error in uploading files for dealership: {}'.format(e.message))
                return jsonify({"status": "ERROR",
                                "message": "Details could not be updated as file can't be uploaded. "
                                           "Please email the document to sales@petromoney.co.in"})
        if id is not None:
            col_list = ["id", "name", "address", "address_2", "pincode", "business_type", "business_property", "doi",
                        "is_microatm", "pan", "gst", "pan_file_url", "gst_file_url", "zone", "region", "location",
                        "district", "state", "latitude", "longtitude", "sales_area", "nhsh", "urh", "auto", "omc",
                        "visit_status", "deal_status"]
            query = mydb._gen_update_query("m_dealership", col_list, data)
            logger.info(query)
            return _is_rows_affected(query + " WHERE id = {}".format(id), True, 1, id)
    elif request.method == 'GET':
        if (id == None):
            return _get_result(query_parser.get('dealership', '_get_all_dealerships'))
        elif (id > 0):
            dealership_details = mydb.run_query(mydb.SELECT,
                                                query_parser.get('dealership', '_get_dealership_by_id').format(id))
            return jsonify({'status': 'SUCCESS', 'data': dealership_details})
        else:
            return jsonify({'status': 'ERROR', 'message': error_parser.get('invalid', '_invalid_request')})
    elif request.method == 'DELETE':
        data = request.get_json()
        if (data.get('type') == 'PAN'):
            mydb.run_query(mydb.UPDATE,
                           query_parser.get('dealership', '_remove_dealership_attachment').format('pan_file_url', id))
            return jsonify({"status": "SUCCESS", "message": "Attachment removed successfully."})
        if (data.get('type') == 'GST'):
            mydb.run_query(mydb.UPDATE,
                           query_parser.get('dealership', '_remove_dealership_attachment').format('gst_file_url', id))
            return jsonify({"status": "SUCCESS", "message": "Attachment removed successfully."})


@app.route('/api/assigned/dealership/<int:fo_id>', methods=['GET'])
@jwt_required
def _get_assigned_dealership(fo_id=None):
    if request.method == 'GET':
        return _get_result(query_parser.get('dealership', '_get_all_dealership_fo').format(fo_id))
    else:
        return jsonify({'status': 'ERROR', 'message': error_parser.get('invalid', '_invalid_request')})


@app.route('/api/dealership/<int:id>/business/details', methods=['GET', 'POST'])
def _business_details_dealership(id=None):
    if id is not None:
        if request.method == 'GET':
            return _get_result(query_parser.get('dealership', '_get_business_details').format(id))
        elif request.method == 'POST':
            data = request.get_json()
            data.update({"dealership_id": id})
            col_list = ["dealership_id", "business_age", "hsd_count", "ms_count", "has_atm", "is_microatm",
                        "fuel_station_area", "electricity_units_month", "electricity_bill_month", "insurance_pump",
                        "insurance_all", "credit_sales_day", "credit_sales_month", "avg_realization_period",
                        "credit_outstanding"]
            query = mydb._gen_upsert_query("t_business_details", col_list, data)
            return _is_rows_affected(query, True, 4, id)
    else:
        return jsonify({'status': 'ERROR', 'message': error_parser.get('invalid', '_id_not_found')})


@app.route('/api/dealership/<int:dealership_id>/credit/info', methods=["POST", "GET"])
@jwt_required
def _credit_info(dealership_id=None):
    if dealership_id is not None:
        if request.method == 'POST':
            data = request.get_json()
            data.update({"dealership_id": dealership_id, "last_modified_by": get_jwt_identity()})
            col_list = ["dealership_id", "dealer_id", "coapplicant_id", "cibil_score", "loans_count",
                        "closed_loans_count",
                        "od_accounts_count", "od_amount", "current_os_amount", "cibil_vintage", "no_of_enquiries",
                        "is_loan_in_bureau", "highest_dpd", "highest_dpd_bracket", "is_cc_in_cibil", "status",
                        "last_modified_by"]
            if (data.get('id') is not None):
                query = mydb._gen_update_query("t_dealership_applicants_credit_info", col_list, data)
                return _execute_query_with_status(mydb.UPDATE, query + " WHERE id={}".format(data.get('id')), True, 10,
                                                  dealership_id)
            else:
                query = mydb._gen_insert_query_exclude_cols("t_dealership_applicants_credit_info", col_list, data)
                return _execute_query_with_status(mydb.INSERT, query, True, 10, dealership_id)
        elif request.method == 'GET':
            return _get_result(query_parser.get('mdm_credit', '_get_credit_info').format(dealership_id))
    else:
        return jsonify({'status': 'ERROR', 'message': error_parser.get('invalid', '_invalid_request')})


@app.route('/api/dealership/<int:dealership_id>/credit/report', methods=["POST", "GET"])
@jwt_required
def _credit_report(dealership_id=None):
    if dealership_id is not None:
        if request.method == 'POST':
            data = request.get_json()
            data.update({"last_modified_by": get_jwt_identity()})
            credit_report_data = _compute_credit_report(data, dealership_id)
            credit_report_data.update({"dealership_id": dealership_id})
            col_list = mydb._get_columns("t_dealership_credit_report")
            if (data.get('id') is not None):
                query = mydb._gen_update_query("t_dealership_credit_report", col_list, credit_report_data)
                return _execute_query_with_status(mydb.UPDATE, query + " WHERE id = {}".format(data.get('id')), True,
                                                  11, dealership_id)
            else:
                query = mydb._gen_insert_query_exclude_cols("t_dealership_credit_report", col_list, data)

                return _execute_query_with_status(mydb.INSERT, query, True, 11, dealership_id)
        elif request.method == 'GET':
            return _get_result(query_parser.get('mdm_credit', '_get_credit_report').format(dealership_id))
    else:
        return jsonify({'status': 'ERROR', 'message': error_parser.get('invalid', '_invalid_request')})


def _compute_credit_report(data, dealership_id):
    business_vintage = int(data.get('business_vintage', 0))
    '''Gross income from fuel'''
    result = mydb.run_query(mydb.SELECT, query_parser.get('dealership', '_get_gross_income_fuel').format(dealership_id))
    gross_income_fuel = foir = 0
    if len(result) == 2:
        gross_income_fuel = result[0].get('gross_income', 0) \
            if result[0].get('gross_income', 0) < result[1].get('gross_income', 0) \
            else dict(reduce(add, map(Counter, result))).get('gross_income', 0) / len(result)
    elif len(result) == 1:
        gross_income_fuel = result[0].get('gross_income', 0)
    data.update({"gross_income_fuel": gross_income_fuel})
    '''Total income of dealership'''
    total_income_res = mydb.run_query(mydb.SELECT,
                                      query_parser.get('dealership', '_get_total_income').format(dealership_id))
    total_income = _if_none_and_sum(total_income_res, 'cur_fy_income')
    data.update({"total_income": total_income})
    '''Total expense of dealership'''
    total_expense_res = mydb.run_query(mydb.SELECT,
                                       query_parser.get('dealership', '_get_total_expense').format(dealership_id))
    total_expense = _if_none_and_sum(total_expense_res, 'expense_amount')
    gross_income_considered = int(gross_income_fuel) + int(total_income)
    ebidta = gross_income_considered - total_expense
    total_obligations = data.get('current_loans_emi', 0) + data.get('interest', 0)
    data.update({"gross_income_considered": gross_income_considered, "total_expense": total_expense,
                 "ebidta": ebidta, "total_obligations": total_obligations})
    if business_vintage < 4:
        foir = 0.7
    elif 4 <= business_vintage <= 6:
        foir = 0.8
    elif 6 <= business_vintage <= 8:
        foir = 0.9
    elif business_vintage > 8:
        foir = 1
    data.update({"foir": foir})
    foir_ebidta = ebidta * foir
    data.update({"foir_ebidta": foir_ebidta})
    is_loan = 1 if foir_ebidta > total_obligations else 0
    data.update({"is_loan": is_loan})
    max_loan_interest = 0 if (foir_ebidta - total_obligations) <= 0 else (foir_ebidta - total_obligations) / 12
    applicable_interest = data.get('applicable_interest', 18)
    max_loan_foir = (max_loan_interest / (applicable_interest / 100)) * 12
    dealership_turnover = mydb.run_query(mydb.SELECT,
                                         query_parser.get('dealership', '_get_dealership_turnover').format(
                                             dealership_id))
    turnover = 0 if _is_empty(dealership_turnover) else dealership_turnover[0].get('turnover', 0)
    annual_turnover = int(turnover)
    loan_percentage = float(data.get('loan_percentage', 1))
    # max_loan_turnover = annual_turnover * (loan_percentage / 100)
    # max_loan_possible = min(float(max_loan_foir), float(max_loan_turnover))
    max_loan_possible = float(max_loan_foir)
    data.update({"max_loan_interest": max_loan_interest, "applicable_interest": applicable_interest,
                 "max_loan_foir": max_loan_foir, "annual_turnover": annual_turnover,
                 "loan_percentage": loan_percentage,
                 "max_loan_turnover": 0, "max_loan_possible": max_loan_possible})
    score = float(data.get('score', 0))
    score_impact = 0
    if 45 < score <= 60:
        score_impact = 0.7
    elif 60 < score <= 75:
        score_impact = 0.8
    elif 75 < score <= 85:
        score_impact = 0.9
    elif score > 85:
        score_impact = 1
    max_loan_exposure = max_loan_possible * score_impact
    max_loan_cap = int(data.get('max_loan_cap', 3000000))
    pm_exposure = data.get('pm_exposure', 0)
    final_loan_value = min(max_loan_exposure, (max_loan_cap - int(pm_exposure)))
    annual_interest = 0
    final_loan_amount = int(data.get('final_loan_amount', 0))
    if final_loan_amount > 0:
        annual_interest = final_loan_amount * (applicable_interest/100)
    foir_percentage = (annual_interest + total_obligations) / ebidta
    data.update({"score": score, "score_impact": score_impact, "max_loan_exposure": max_loan_exposure,
                 "max_loan_cap": max_loan_cap, "pm_exposure": pm_exposure, "final_loan_value": final_loan_value,
                 "annual_interest": annual_interest, "foir_percentage": foir_percentage})
    return data


@app.route('/api/dealership/<int:id>/income/details', methods=['GET', 'POST'])
def _get_income_details_dealership(id=None):
    if id is not None:
        if request.method == 'GET':
            return _get_result(query_parser.get('dealership', '_get_income_details').format(id))
        elif request.method == 'POST':
            data = request.get_json()
            user_id = data.get('user_id', 0)
            return _is_rows_affected(query_parser.get('dealership', '_post_income_details')
                                     .format(id, data.get('business_name'), data.get('business_type', 1),
                                             data.get('business_owner', user_id),
                                             data.get('business_age', 0), data.get('cur_fy_turnover', 0),
                                             data.get('cur_fy_profit_loss', 0), data.get('cur_fy_income', 0)),
                                     True, 5, id)
        else:
            return jsonify({'status': 'ERROR', 'message': error_parser.get('invalid', '_invalid_request')})
    else:
        return jsonify({'status': 'ERROR', 'message': error_parser.get('invalid', '_id_not_found')})


@app.route('/api/dealership/income/details/<int:id>', methods=['POST'])
def _update_other_income_details_dealership(id=None):
    if id is not None:
        if request.method == 'POST':
            data = request.get_json()
            cols = ["dealership_id", "business_name", "business_type", "business_owner", "business_age",
                    "cur_fy_turnover", "cur_fy_profit_loss", "cur_fy_income"]
            query = mydb._gen_update_query("t_dealership_other_income", cols, data)
            return _is_rows_affected(query + " WHERE id = {}".format(id))
        else:
            return jsonify({'status': 'ERROR', 'message': error_parser.get('invalid', '_invalid_request')}), 200
    else:
        return jsonify({'status': 'ERROR', 'message': error_parser.get('invalid', '_id_not_found')}), 200


@app.route('/api/dealership/<int:id>/expense/details', methods=['GET', 'POST'])
def _get_expense_details_dealership(id=None):
    if id is not None:
        if request.method == 'GET':
            return _get_result(query_parser.get('dealership', '_get_expense_details').format(id))
        elif request.method == 'POST':
            data = request.get_json()
            return _is_rows_affected(query_parser.get('dealership', '_post_expense_details')
                                     .format(id, data.get('expense_type'), data.get('expense_amount')), True, 5, id)
        else:
            return jsonify({'status': 'ERROR', 'message': error_parser.get('invalid', '_invalid_request')}), 200
    else:
        return jsonify({'status': 'ERROR', 'message': error_parser.get('invalid', '_id_not_found')}), 200


@app.route('/api/dealership/expense/details/<int:id>', methods=['POST'])
def _update_other_expense_details_dealership(id=None):
    if id is not None:
        if request.method == 'POST':
            data = request.get_json()
            return _is_rows_affected(query_parser.get('dealership', '_update_expense_details')
                                     .format(data.get('expense_type'), data.get('expense_amount'), id))
        else:
            return jsonify({'status': 'ERROR', 'message': error_parser.get('invalid', '_invalid_request')}), 200
    else:
        return jsonify({'status': 'ERROR', 'message': error_parser.get('invalid', '_id_not_found')}), 200


@app.route('/api/dealership/<int:id>/assets', methods=['GET', 'POST'])
def _get_asset_details_dealership(id=None):
    if id is not None:
        if request.method == 'GET':
            return _get_result(query_parser.get('dealership', '_get_asset_details').format(id))
        elif request.method == 'POST':
            data = request.get_json()
            return _is_rows_affected(query_parser.get('dealership', '_post_asset_details')
                                     .format(id, data.get('asset_description'), data.get('cost'),
                                             data.get('market_value'), data.get('ownership'),
                                             data.get('ownership_proof'),
                                             data.get('comments')), True, 6, id)
        else:
            return jsonify({'status': 'ERROR', 'message': error_parser.get('invalid', '_invalid_request')}), 200
    else:
        return jsonify({'status': 'ERROR', 'message': error_parser.get('invalid', '_id_not_found')}), 200


@app.route('/api/dealership/assets/details/<int:id>', methods=['POST'])
def _update_asset_details_dealership(id=None):
    if id is not None:
        if request.method == 'POST':
            data = request.get_json()
            return _is_rows_affected(query_parser.get('dealership', '_update_asset_details')
                                     .format(data.get('asset_description'), data.get('cost'),
                                             data.get('market_value'), data.get('ownership'),
                                             data.get('ownership_proof'),
                                             data.get('comments'), id))
        else:
            return jsonify({'status': 'ERROR', 'message': error_parser.get('invalid', '_invalid_request')}), 200
    else:
        return jsonify({'status': 'ERROR', 'message': error_parser.get('invalid', '_id_not_found')}), 200


@app.route('/api/dealership/<int:id>/bank', methods=['GET', 'POST'])
def _get_bank_details_dealership(id=None):
    if id is not None:
        if request.method == 'GET':
            return _get_result(query_parser.get('dealership', '_get_bank_details').format(id))
        elif request.method == 'POST':
            data = request.get_json()
            return _is_rows_affected(query_parser.get('dealership', '_post_bank_details')
                                     .format(id, data.get('account_name'), data.get('account_no'),
                                             data.get('account_type'), data.get('bank_name'),
                                             data.get('bank_branch', None), data.get('bank_city', None),
                                             data.get('ifsc', None), data.get('micr', None), data.get('swift', None),
                                             data.get('account_since', None)), True, 7, id)
        else:
            return jsonify({'status': 'ERROR', 'message': error_parser.get('invalid', '_invalid_request')}), 200
    else:
        return jsonify({'status': 'ERROR', 'message': error_parser.get('invalid', '_id_not_found')}), 200


@app.route('/api/dealership/bank/<int:id>', methods=['POST'])
def _update_bank_details_dealership(id=None):
    if id is not None:
        if request.method == 'POST':
            data = request.get_json()
            return _is_rows_affected(query_parser.get('dealership', '_update_bank_details')
                                     .format(data.get('account_name'), data.get('account_no'), data.get('account_type'),
                                             data.get('bank_name'),
                                             data.get('ifsc', None), data.get('micr', None), data.get('swift', None),
                                             data.get('bank_branch', None), data.get('bank_city', None),
                                             data.get('account_since', None), id))
        else:
            return jsonify({'status': 'ERROR', 'message': error_parser.get('invalid', '_invalid_request')}), 200
    else:
        return jsonify({'status': 'ERROR', 'message': error_parser.get('invalid', '_id_not_found')}), 200


@app.route('/api/application/state', methods=['GET'])
@jwt_required
def get_application_state():
    _application = mydb.run_query(mydb.SELECT, query_parser.get('dealership', '_get_all_application'))
    return jsonify({"status": "SUCCESS", "data": _application})


@app.route('/api/dealership/<int:dealership_id>/loans', methods=['GET', 'POST'])
@app.route('/api/dealership/<int:dealership_id>/loans/<int:loan_id>', methods=['GET', 'POST', 'DELETE'])
def _dealership_loan_details(dealership_id=None, loan_id=None):
    if dealership_id is not None:
        if loan_id is not None:
            if request.method == 'POST':
                data = request.get_json()
                _get_current_loan_status = mydb.run_query(mydb.SELECT,
                                                          "SELECT status from t_dealership_loans WHERE id = {}".format(
                                                              loan_id))
                logger.debug(_get_current_loan_status)
                now = datetime.datetime.now()
                if (data.get('status') == "approved" and (
                        _get_current_loan_status[0].get('status')).lower() == "submitted"):
                    data.update({'loan_approved_rejected_date': now.strftime('%Y-%m-%d %H:%M:%S')})
                elif (data.get('status') == "disbursed" and _get_current_loan_status[0].get('status') == "approved"):
                    data.update({'loan_disbursed_date': now.strftime('%Y-%m-%d %H:%M:%S')})
                elif (data.get('status') == "disbursed" and _get_current_loan_status[0].get('status') == "submitted"):
                    return jsonify(error_parser.get('invalid', '_incorrect_loan_process')), 200
                cols = mydb._get_columns("t_dealership_loans")
                query = mydb._gen_update_query("t_dealership_loans", cols, data)
                return _is_rows_affected(
                    query + ", remarks=CONCAT(remarks, '{0}') WHERE id = {1} AND dealership_id = {2}".format(
                        data.get('remarks'), loan_id, dealership_id), True, 3, dealership_id)
            elif request.method == 'DELETE':
                return _is_rows_affected(
                    query_parser.get('dealership', '_remove_loan_details').format(loan_id, dealership_id), False, 3,
                    dealership_id)
            elif request.method == 'GET':
                loan_data = mydb.run_query(mydb.SELECT,
                                           query_parser.get('dealership', '_get_loan_details_by_id').format(loan_id,
                                                                                                            dealership_id))
                loan_disbursed_details_data = mydb.run_query(mydb.SELECT,
                                                             query_parser.get('dealership',
                                                                              '_get_loan_disbursement_details').format(
                                                                 loan_id))
                logger.debug("loan_disbursed_details_data: {}".format(loan_disbursed_details_data))
                for key, value in groupby(loan_disbursed_details_data, key=itemgetter('applicant_code')):
                    result = dict()
                    result.update({'applicant_code': key})
                    details = list()
                    for i in value:
                        details.append(i)
                    result.update({'disbursement_details': details})
                    loan_data[0].update(result)
                return jsonify({"status": "SUCCESS", "data": loan_data})
            else:
                return jsonify(error_parser.get('invalid', '_invalid_request')), 200
        else:
            if request.method == 'POST':
                if _dealership_loan_exists(dealership_id):
                    return jsonify({"status": "ERROR", "message": error_parser.get('invalid', '_loan_exists')})
                else:
                    data = request.get_json()
                    data.update({'dealership_id': dealership_id})
                    _query = mydb._gen_insert_query('t_dealership_loans', data)
                    return _execute_query_with_status(mydb.INSERT, _query, True, 3, dealership_id)
            elif request.method == 'GET':
                return _get_result(query_parser.get('dealership', '_get_dealership_loan_details').format(dealership_id))
    else:
        return jsonify({"status": "ERROR", "message": error_parser.get('invalid', '_id_not_found')})


@app.route('/api/dealership/<int:dealership_id>/operators', methods=['POST', 'GET'])
@app.route('/api/dealership/<int:dealership_id>/operators/<int:id>', methods=['POST', 'GET', 'DELETE'])
@jwt_required
def _fleet_operators(id=None, dealership_id=None):
    if request.method == 'POST':
        data = request.get_json()
        data.update({'dealership_id': dealership_id})
        if id is not None:
            cols = mydb._get_columns('t_fleet_operators')
            _query = mydb._gen_update_query('t_fleet_operators', cols, data) + ' where id ={}'.format(id)
            result = mydb.run_query(mydb.UPDATE, _query)
            return jsonify({"status": "SUCCESS", "message": "Updated successfully.", "result": result})
        else:
            return _execute_query(mydb.INSERT, mydb._gen_insert_query('t_fleet_operators', data))
    elif request.method == 'GET':
        if id is not None:
            return _get_result(
                query_parser.get('dealership_transporter', '_get_fleet_operators_by_id').format(id))
        else:
            return _get_result(
                query_parser.get('dealership_transporter', '_get_fleet_operators_by_dealership_id').format(
                    dealership_id))
    elif request.method == 'DELETE':
        _query = query_parser.get('dealership_transporter', '_del_fleet_operators_by_id').format(id)
        result = mydb.run_query(mydb.DELETE, _query)
        return jsonify({"status": "SUCCESS", "message": "Removed successfully.", "result": result})


@app.route('/api/loans', methods=['GET'])
def _get_all_loans():
    status = request.args.get('status')
    if (status is not None and status != ''):
        _status_list = status.split(",")
        _status_list = tuple(_status_list) if (len(_status_list) > 1) else "\'" + status + "\'"
        query = (query_parser.get('dealership',
                                  '_get_all_loans') + " WHERE status={} AND lp.product_name LIKE \"%FUEL%\" ORDER BY tdl.modified_date DESC").format(
            _status_list)
        return _get_result(query)
    else:
        query = (query_parser.get('dealership', '_get_all_loans') + " WHERE lp.product_name LIKE \"%FUEL%\"")
        return _get_result(query)


@app.route('/api/loans/sanction', methods=['GET'])
@app.route('/api/loans/<int:loan_id>/sanction', methods=['GET'])
def _get_sanction_letter(loan_id=None):
    _query = "SELECT id, dealership_id from t_dealership_loans WHERE status=\"submitted\""
    if loan_id is None:
        logger.debug(_query)
        return _get_result(_query)
    else:
        logger.debug(_query + " AND id=" + str(loan_id))
        data = mydb.run_query(mydb.SELECT, _query + " AND id=" + str(loan_id))
        logger.debug(data)
        return _generate_sanction_letter(data[0].get('dealership_id'), data[0].get('id'))


def _generate_sanction_letter(dealership_id=None, loan_id=None):
    logger.debug("Generating sanction letter for approved loan, id:{}".format(str(loan_id)))
    _cur_date = datetime.date.today()
    query = query_parser.get('dealership', '_loan_sanction_letter').format(dealership_id)
    data = mydb.run_query(mydb.SELECT, query)
    if data:
        logger.debug("Data in request ----> %s", data[0])
        data[0].update({"date": "{}-{}-{}".format(_cur_date.day, _cur_date.month, _cur_date.year)})
        data = data[0]
        logger.debug("Sanction letter data: %s", data)
        _sanction_letter_template = app.config['SANCTION_LETTER_TEMPLATE']
        _target_file_path = app.config['TARGET_FILE_PATH'] + "/{}/".format(str(dealership_id))
        _file_name = "{}_sanction_letter.html".format(dealership_id)
        _sanction_letter = _find_and_replace(_sanction_letter_template, _target_file_path, _file_name, data)
        logger.debug("Replaced file %s", _sanction_letter)
        options.update({'header-html': 'template/header.html'})
        with open(_sanction_letter) as fin:
            logger.debug("Generating sanction letter...")
            pdfkit.from_file(fin, os.path.join(_target_file_path, "sanction_letter.pdf"), options=options,
                             configuration=_wkhtml_config)
        logger.debug("Sanction letter is being uploaded to S3......")
        with open(os.path.join(_target_file_path, "sanction_letter.pdf"), errors='ignore') as new_file:
            _upload_status = _s3_file_upload(str(dealership_id) + "/sanction/sanction_letter.pdf",
                                             dealership_docs_bucket, new_file)
            if _upload_status:
                _sanction_letter_url = docs_url + "/" + str(
                    dealership_id) + "/sanction/sanction_letter.pdf"
                logger.debug("Uploaded sanction letter to path %s", _sanction_letter_url)
                result = mydb.run_query(mydb.UPDATE,
                                        "UPDATE t_dealership_loans SET sanction_letter_url='{0}' WHERE id={1}".format(
                                            _sanction_letter_url, loan_id))
                logger.debug("Sanction letter URL update result: {}".format(result))
                return jsonify({"status": "SUCCESS", "message": "Sanction letter generated successfully",
                                "data": {"sanction_letter_url": _sanction_letter_url}})
            else:
                logger.debug("Upload Error: Sanction letter could not be uploaded to S3. Please try again.")
                return jsonify({"status": "ERROR",
                                "message": "Sanction letter generated successfully but could not be uloaded to S3. "
                                           "Please contact administrator."})
    else:
        return jsonify(
            {"status": "ERROR", "message": "Sanction letter could not be generated. Please contact administrator."})


@app.route('/api/loans/exceptions', methods=['GET'])
@jwt_required
def _map_lms_with_los():
    return jsonify(
        {"status": "SUCCESS", "data": _get_data_from_lms("SELECT APPLICANT_CODE, APPLICANT_NAME FROM vpm_exception")})


@app.route('/api/dealers/<int:dealership_id>', methods=['GET', 'POST'])
@app.route('/api/dealers/<int:dealership_id>/<int:id>', methods=['GET', 'POST', 'DELETE'])
@jwt_required
def _dealers(dealership_id=None, id=None):
    data = dict()
    if request.method == 'POST':
        data = dict(request.form)
        data.update({"dealership_id": dealership_id})
        data.update({"last_modified_by": get_jwt_identity()})
        logger.debug("Dealers request data: %s", data)

    if request.files is not None:
        try:
            for file in request.files:
                logger.debug("File in dealers request object : {}".format(file))
                if (file == 'profile_image_url'):
                    _status, profile_image_url = _file_upload(request.files.getlist(file), dealership_id, doc_id=1)
                    logger.debug("profile image url: %s", profile_image_url)
                    data.update({'profile_image_url': profile_image_url})
                if (file == 'pan_file_url'):
                    _status, pan_file_url = _file_upload(request.files.getlist(file), dealership_id, doc_id=2)
                    logger.debug("pan file url: %s", pan_file_url)
                    data.update({'pan_file_url': pan_file_url})
                if (file == 'aadhar_f_file_url'):
                    _status, aadhar_f_file_url = _file_upload(request.files.getlist(file), dealership_id, doc_id=3)
                    logger.debug("aadhar front file url: %s", aadhar_f_file_url)
                    data.update({'aadhar_f_file_url': aadhar_f_file_url})
                if (file == 'aadhar_b_file_url'):
                    _status, aadhar_b_file_url = _file_upload(request.files.getlist(file), dealership_id, doc_id=3)
                    logger.debug("aadhar back file url: %s", aadhar_b_file_url)
                    data.update({'aadhar_b_file_url': aadhar_b_file_url})
        except OSError as e:
            logger.debug('Error: {}'.format(e.message))
            return jsonify({"status": "ERROR",
                            "message": "Details could not be updated as file can't be uploaded. "
                                       "Please email the document to sales@petromoney.co.in"})
    # _col_list = ["id", "dealership_id", "first_name", "last_name", "dob", "gender", "marital_status", "email", "mobile",
    #              "is_aadhar_linked", "is_whatsapp", "profile_image_url", "aadhar", "pan", "pan_file_url",
    #              "aadhar_f_file_url", "aadhar_b_file_url", "address", "residing_since", "is_main_applicant"]
    _col_list = mydb._get_columns('m_dealers')
    if id is not None:
        data.update({"id": id})
        if request.method == 'POST':
            query = mydb._gen_update_query("m_dealers", _col_list, data)
            return _is_rows_affected(query + " WHERE id=" + str(id), True, 2, dealership_id)
        elif request.method == 'GET':
            dealer_details = mydb.run_query(mydb.SELECT, query_parser.get('dealers', '_get_dealers_id').format(id))
            return jsonify({'status': 'SUCCESS', 'data': dealer_details})
        elif request.method == 'DELETE':
            data = request.get_json()
            if (data.get('type') == 'PAN'):
                mydb.run_query(mydb.UPDATE,
                               query_parser.get('dealers', '_remove_attachment').format('pan_file_url', dealership_id,
                                                                                        id))
                return jsonify({"status": "SUCCESS", "message": "Attachment removed successfully."})
            if (data.get('type') == 'AADHAR_FRONT'):
                mydb.run_query(mydb.UPDATE,
                               query_parser.get('dealers', '_remove_attachment').format('aadhar_f_file_url',
                                                                                        dealership_id, id))
                return jsonify({"status": "SUCCESS", "message": "Attachment removed successfully."})
            if (data.get('type') == 'AADHAR_BACK'):
                mydb.run_query(mydb.UPDATE,
                               query_parser.get('dealers', '_remove_attachment').format('aadhar_b_file_url',
                                                                                        dealership_id, id))
                return jsonify({"status": "SUCCESS", "message": "Attachment removed successfully."})
            if (data.get('type') == 'PROFILE_IMAGE'):
                mydb.run_query(mydb.UPDATE,
                               query_parser.get('dealers', '_remove_attachment').format(
                                   'profile_image_url', id))
                return jsonify({"status": "SUCCESS", "message": "Profile image removed successfully."})
    else:
        if request.method == 'POST':
            if _is_main_applicant(data.get('mobile'), dealership_id):
                logger.debug("Mobile number already exists.")
                return jsonify(error_parser.get('invalid', '_mobile_no_exists'))
            else:
                data.update({"is_main_applicant": "1"})
            insert_dealer_data = mydb._gen_insert_query_exclude_cols("m_dealers", _col_list, data)
            return _is_rows_affected(insert_dealer_data, True, 2, dealership_id)
        elif request.method == 'GET':
            return _get_result(query_parser.get('dealers', '_get_associated_dealers').format(dealership_id))


@app.route('/api/applicants/dealership/<int:dealership_id>', methods=['GET'])
def _get_all_applicants(dealership_id=None):
    if dealership_id is not None:
        applicants_data = dict()
        data_list = list()
        co_applicants = dict()
        co_applicants_list = list()
        all_applicants = mydb.run_query(mydb.SELECT,
                                        query_parser.get('dealership', '_get_all_applicants').format(dealership_id))
        applicants_group = groupby(all_applicants, key=lambda each: each['main_applicant_id'])
        for main_applicant_id, main_applicant_names in applicants_group:
            applicants_data.update({"main_applicant_id": main_applicant_id})
            for applicants in main_applicant_names:
                if (applicants.get('co_applicant_id') is not None):
                    co_applicants.update({"co_applicant_id": applicants.get('co_applicant_id'),
                                          "co_applicant_name": applicants.get('co_applicant_name'),
                                          "relationship": applicants.get('relationship')})
                    co_applicants_list.append(co_applicants.copy())
                applicants_data.update({"main_applicant_name": applicants.get('main_applicant_name')})
                applicants_data.update({"co_applicants": co_applicants_list.copy()})
            co_applicants_list.clear()
            data_list.append(applicants_data.copy())
        return jsonify({'status': 'SUCCESS', 'data': data_list})
    else:
        return jsonify(error_parser.get('invalid', '_id_not_found')), 200


@app.route('/api/coapplicants/<int:dealership_id>', methods=['GET', 'POST'])
@app.route('/api/coapplicants/<int:dealership_id>/<int:id>', methods=['GET', 'POST', 'DELETE'])
@jwt_required
def _dealer_co_applicants(dealership_id=None, id=None):
    if request.method == 'POST':
        data = dict(request.form)
        data.update({"last_modified_by": get_jwt_identity()})
        logger.debug("Co-applicant data in request:\n {}".format(data))
    try:
        logger.debug("Request files...")
        for file in request.files:
            logger.debug(file)
            if (file == 'profile_image_url'):
                _status, profile_image_url = _file_upload(request.files.getlist(file), dealership_id, doc_id=1)
                data.update({'profile_image_url': profile_image_url})
            if (file == 'pan_file_url'):
                _status, pan_file_url = _file_upload(request.files.getlist(file), dealership_id, doc_id=2)
                data.update({'pan_file_url': pan_file_url})
            if (file == 'aadhar_f_file_url'):
                _status, aadhar_f_file_url = _file_upload(request.files.getlist(file), dealership_id, doc_id=3)
                data.update({'aadhar_f_file_url': aadhar_f_file_url})
            if (file == 'aadhar_b_file_url'):
                _status, aadhar_b_file_url = _file_upload(request.files.getlist(file), dealership_id, doc_id=3)
                data.update({'aadhar_b_file_url': aadhar_b_file_url})
    except OSError as e:
        logger.debug('Error: {}'.format(e.message))
        return jsonify({"status": "FAILURE",
                        "message": "File could not be uploaded. Please email the document to sales@petromoney.co.in"})
    col_list = mydb._get_columns('t_dealers_coapplicants')
    if id is not None:
        if request.method == 'POST':
            query = mydb._gen_update_query('t_dealers_coapplicants', col_list, data)
            return _is_rows_affected(query + " WHERE id={}".format(id), True, 8, dealership_id)
        elif request.method == 'GET':
            dealer_details = mydb.run_query(mydb.SELECT,
                                            query_parser.get('coapplicants', '_get_coapplicant_by_id').format(id))
            return jsonify({'status': 'SUCCESS', 'data': dealer_details})
        elif request.method == 'DELETE':
            data = request.get_json()
            if (data.get('type') == 'PAN'):
                mydb.run_query(mydb.UPDATE,
                               query_parser.get('coapplicants', '_remove_coapplicant_attachment').format('pan_file_url',
                                                                                                         id))
                return jsonify({"status": "SUCCESS", "message": "Attachment removed successfully."})
            if (data.get('type') == 'AADHAR_FRONT'):
                mydb.run_query(mydb.UPDATE,
                               query_parser.get('coapplicants', '_remove_coapplicant_attachment').format(
                                   'aadhar_f_file_url', id))
                return jsonify({"status": "SUCCESS", "message": "Attachment removed successfully."})
            if (data.get('type') == 'AADHAR_BACK'):
                mydb.run_query(mydb.UPDATE,
                               query_parser.get('coapplicants', '_remove_coapplicant_attachment').format(
                                   'aadhar_b_file_url', id))
                return jsonify({"status": "SUCCESS", "message": "Attachment removed successfully."})
            if (data.get('type') == 'PROFILE_IMAGE'):
                mydb.run_query(mydb.UPDATE,
                               query_parser.get('coapplicants', '_remove_coapplicant_attachment').format(
                                   'profile_image_url', id))
                return jsonify({"status": "SUCCESS", "message": "Profile image removed successfully."})
    else:
        if request.method == 'POST':
            query = mydb._gen_insert_query_exclude_cols("t_dealers_coapplicants", col_list, data)
            logger.debug("coapplicant insert query: %s", query)
            return _is_rows_affected(query, True, 8, dealership_id)
        elif request.method == 'GET':
            return _get_result(query_parser.get('coapplicants', '_get_associated_coapplicants').format(dealership_id))


@app.route('/api/guarantors/<int:dealership_id>', methods=['GET', 'POST'])
@app.route('/api/guarantors/<int:dealership_id>/<int:id>', methods=['GET', 'POST', 'DELETE'])
@jwt_required
def _dealership_guarantors(dealership_id=None, id=None):
    if request.method == 'POST':
        data = dict(request.form)
        logger.debug("Guarantor data in request:\n {}".format(data))
        data.update({"dealership_id": dealership_id})
        data.update({'last_modified_by': get_jwt_identity()})
        logger.debug("Dealership guarantors data: {}".format(data))
    try:
        if request.files:
            logger.debug("Uploading files of the guarantor....")
            for file in request.files:
                logger.debug(file)
                if (file == 'profile_image_url'):
                    _status, profile_image_url = _file_upload(request.files.getlist(file), dealership_id, doc_id=1)
                    data.update({'profile_image_url': profile_image_url})
                if (file == 'pan_file_url'):
                    _status, pan_file_url = _file_upload(request.files.getlist(file), dealership_id, doc_id=2)
                    data.update({'pan_file_url': pan_file_url})
                if (file == 'aadhar_f_file_url'):
                    _status, aadhar_f_file_url = _file_upload(request.files.getlist(file), dealership_id, doc_id=3)
                    data.update({'aadhar_f_file_url': aadhar_f_file_url})
                if (file == 'aadhar_b_file_url'):
                    _status, aadhar_b_file_url = _file_upload(request.files.getlist(file), dealership_id, doc_id=3)
                    data.update({'aadhar_b_file_url': aadhar_b_file_url})
    except OSError as e:
        logger.debug('Error: {}'.format(e.message))
        return jsonify({"status": "FAILURE",
                        "message": "File could not be uploaded. Please email the document to sales@petromoney.co.in"})

    if id is not None:
        if request.method == 'POST':
            col_list = mydb._get_columns('t_dealership_guarantors')
            query = mydb._gen_update_query('t_dealership_guarantors', col_list, data)
            return _is_rows_affected(query + " WHERE id={}".format(id), True, 15, dealership_id)
        elif request.method == 'GET':
            dealer_details = mydb.run_query(mydb.SELECT,
                                            query_parser.get('guarantors', '_get_guarantor_by_id').format(id))
            return jsonify({'status': 'SUCCESS', 'data': dealer_details})
        elif request.method == 'DELETE':
            data = request.get_json()
            if data.get('type') is not None:
                _remove_attachment_query = query_parser.get('guarantors', '_remove_guarantor_attachment')

                if (data.get('type') == 'PAN'):
                    _column_key = 'pan_file_url'
                if (data.get('type') == 'AADHAR_FRONT'):
                    _column_key = 'aadhar_f_file_url'
                if (data.get('type') == 'AADHAR_BACK'):
                    _column_key = 'aadhar_b_file_url'
                if (data.get('type') == 'PROFILE_IMAGE'):
                    _column_key = 'profile_image_url'

                return _execute_query(mydb.UPDATE, _remove_attachment_query.format(_column_key, id))
            elif data.get('status') == 0:
                mydb.run_query(mydb.UPDATE, query_parser.get('guarantors', '_disable_guarantor').format(id))
                return jsonify({"status": "SUCCESS", "message": "User removed successfully."})
    else:
        if request.method == 'POST':
            query = mydb._gen_insert_query("t_dealership_guarantors", data)
            logger.debug("Guarantor insert query: %s", query)
            return _is_rows_affected(query, True, 15, dealership_id)
        elif request.method == 'GET':
            return _get_result(
                query_parser.get('guarantors', '_get_associated_guarantors').format(dealership_id))


@app.route('/api/refresh/experian/report/consumer/<int:id>', methods=['GET'])
@jwt_required
def _refresh_experian_report(id=None):
    if id is not None:
        try:
            query = (query_parser.get('experian', '_get_dealers_id').format(id))
            dealer_details = mydb.run_query(mydb.SELECT, query)
            dealer_details = dealer_details[0]
            # If dealer not found send error message
            if not dealer_details:
                return jsonify({"status": "SUCCESS", "message": error_parser.get('invalid', '_id_details_missing')})
            dealer_details.update({"dob": dealer_details.get('dob').replace("/", "")})
            logger.debug(dealer_details)
            state_code_query = (query_parser.get('experian', '_get_state_code').format(dealer_details.get('state')))
            state_code = mydb.run_query(mydb.SELECT, state_code_query)[0]
            dealer_details.update(state_code)
            _target_file_path = app.config['TARGET_FILE_PATH'] + "/{}/".format(str(dealer_details.get('id')))
            _file_name = '{}_experian_payload_file.xml'.format(id)
            _experian_payload = _find_and_replace(app.config['EXPERIAN_TEMPLATE'], _target_file_path, _file_name,
                                                  dealer_details)
        except IOError:
            logger.debug("Error: File does not exist.")
            return jsonify({"status": "SUCCESS", "message": error_parser.get('invalid', '_file_not_found')})

        if _experian_payload is not None:
            with open(_experian_payload) as payload:
                try:
                    _url = "https://connect.experian.in:443/nextgen-ind-pds-webservices-cbv2/endpoint"
                    _header = {'Content-Type': 'application/xml'}

                    response = req.post(_url, data=payload, headers=_header)
                    response_text = response.text
                    # parse and format response text to proper xml
                    response_text = response_text.replace("&lt;", "<")
                    response_text = response_text.replace("&gt;", ">")
                    response_text = response_text.replace(
                        "<?xml version=\"1.0\" encoding=\"UTF-8\" standalone=\"yes\"?>", "")
                    response_text = '<?xml version="1.0" encoding="UTF-8" standalone="yes"?>' + response_text
                    # convert to dict
                    experian_data = xmltodict.parse(response_text)

                    data_dict = dict()
                    data_dict.update({"id": dealer_details.get("id")})
                    data_dict.update({"type": "dealer"})
                    data_dict.update({"last_name": dealer_details.get("last_name")})
                    data_dict.update({"first_name": dealer_details.get("first_name")})
                    data_dict.update({"gender_code": experian_data.get("SOAP-ENV:Envelope").get("SOAP-ENV:Body").get(
                        "ns2:processResponse").get("ns2:out").get("INProfileResponse").get("CAPS").get(
                        "CAPS_Application_Details")[0].get("CAPS_Applicant_Details").get("Gender_Code")})
                    data_dict.update({"pan": dealer_details.get("pan")})
                    data_dict.update({"dob": dealer_details.get("dob")})
                    data_dict.update({"telephone": "0"})
                    data_dict.update({"mobile": dealer_details.get("mobile")})
                    data_dict.update({"email": dealer_details.get("email")})
                    data_dict.update({"bureau_score": experian_data.get("SOAP-ENV:Envelope").get("SOAP-ENV:Body").get(
                        "ns2:processResponse").get("ns2:out").get("INProfileResponse").get("SCORE").get("BureauScore")})
                    data_dict.update({"bureau_score_confidence": experian_data.get("SOAP-ENV:Envelope").get(
                        "SOAP-ENV:Body").get("ns2:processResponse").get("ns2:out").get("INProfileResponse").get(
                        "SCORE").get("BureauScoreConfidLevel")})
                    data_dict.update({"credit_rating": experian_data.get("SOAP-ENV:Envelope").get("SOAP-ENV:Body").get(
                        "ns2:processResponse").get("ns2:out").get("INProfileResponse").get("SCORE").get(
                        "CreditRating")})
                    data_dict.update({"ca_total": experian_data.get("SOAP-ENV:Envelope").get("SOAP-ENV:Body").get(
                        "ns2:processResponse").get("ns2:out").get("INProfileResponse").get("CAIS_Account").get(
                        "CAIS_Summary").get("Credit_Account").get("CreditAccountTotal")})
                    data_dict.update({"ca_active": experian_data.get("SOAP-ENV:Envelope").get("SOAP-ENV:Body").get(
                        "ns2:processResponse").get("ns2:out").get("INProfileResponse").get("CAIS_Account").get(
                        "CAIS_Summary").get("Credit_Account").get("CreditAccountActive")})
                    data_dict.update({"ca_default": experian_data.get("SOAP-ENV:Envelope").get("SOAP-ENV:Body").get(
                        "ns2:processResponse").get("ns2:out").get("INProfileResponse").get("CAIS_Account").get(
                        "CAIS_Summary").get("Credit_Account").get("CreditAccountDefault")})
                    data_dict.update({"ca_closed": experian_data.get("SOAP-ENV:Envelope").get("SOAP-ENV:Body").get(
                        "ns2:processResponse").get("ns2:out").get("INProfileResponse").get("CAIS_Account").get(
                        "CAIS_Summary").get("Credit_Account").get("CreditAccountClosed")})
                    data_dict.update({"ca_suit_file_current_balance": experian_data.get("SOAP-ENV:Envelope").get(
                        "SOAP-ENV:Body").get("ns2:processResponse").get("ns2:out").get("INProfileResponse").get(
                        "CAIS_Account").get("CAIS_Summary").get("Credit_Account").get("CADSuitFiledCurrentBalance")})
                    data_dict.update({"os_balance_secured": experian_data.get("SOAP-ENV:Envelope").get(
                        "SOAP-ENV:Body").get("ns2:processResponse").get("ns2:out").get("INProfileResponse").get(
                        "CAIS_Account").get("CAIS_Summary").get("Total_Outstanding_Balance").get(
                        "Outstanding_Balance_Secured")})
                    data_dict.update({"os_balance_secured_percentage": experian_data.get("SOAP-ENV:Envelope").get(
                        "SOAP-ENV:Body").get("ns2:processResponse").get("ns2:out").get("INProfileResponse").get(
                        "CAIS_Account").get("CAIS_Summary").get("Total_Outstanding_Balance").get(
                        "Outstanding_Balance_Secured_Percentage")})
                    data_dict.update({"os_balance_unsecured": experian_data.get("SOAP-ENV:Envelope").get(
                        "SOAP-ENV:Body").get("ns2:processResponse").get("ns2:out").get("INProfileResponse").get(
                        "CAIS_Account").get("CAIS_Summary").get("Total_Outstanding_Balance").get(
                        "Outstanding_Balance_UnSecured")})
                    data_dict.update({"os_balance_unsecured_percentage": experian_data.get("SOAP-ENV:Envelope").get(
                        "SOAP-ENV:Body").get("ns2:processResponse").get("ns2:out").get("INProfileResponse").get(
                        "CAIS_Account").get("CAIS_Summary").get("Total_Outstanding_Balance").get(
                        "Outstanding_Balance_UnSecured_Percentage")})
                    data_dict.update({"os_balance_all": experian_data.get("SOAP-ENV:Envelope").get("SOAP-ENV:Body").get(
                        "ns2:processResponse").get("ns2:out").get("INProfileResponse").get("CAIS_Account").get(
                        "CAIS_Summary").get("Total_Outstanding_Balance").get("Outstanding_Balance_All")})
                    data_dict.update({"last_modified_by": get_jwt_identity()})

                    experian_data_cais_list = experian_data.get("SOAP-ENV:Envelope").get("SOAP-ENV:Body").get(
                        "ns2:processResponse").get("ns2:out").get("INProfileResponse").get("CAIS_Account").get(
                        "CAIS_ACCOUNT_DETAILS")
                    CAIS_accounts_list = []
                    for cais_account in experian_data_cais_list:
                        cais_account_dict = dict()
                        cais_account_dict.update({"account_type": cais_account.get("Account_Type")})
                        cais_account_dict.update({"open_date": cais_account.get("Open_Date")})
                        cais_account_dict.update({"credit_limit": cais_account.get("Credit_Limit_Amount")})
                        cais_account_dict.update(
                            {"highest_credit": cais_account.get("Highest_Credit_or_Original_Loan_Amount")})
                        cais_account_dict.update({"account_status": cais_account.get("Account_Status")})
                        cais_account_dict.update({"current_balance": cais_account.get("Current_Balance")})
                        cais_account_dict.update({"amount_past_due": cais_account.get("Amount_Past_Due")})
                        cais_account_dict.update({"date_closed": cais_account.get("Date_Closed")})
                        cais_account_dict.update({"days_past_due": 0})
                        cais_account_dict.update({"last_modified_by": get_jwt_identity()})
                        CAIS_accounts_list.append(cais_account_dict)
                    logger.debug(data_dict)
                    logger.debug(CAIS_accounts_list)

                    _experian_insert_query = mydb._gen_insert_query("t_experian_report", data_dict)
                    _query_status = mydb.run_query(mydb.INSERT, _experian_insert_query)
                    return jsonify({"status": "SUCCESS", "message": "Experian data loaded successfully",
                                    "query_status": _query_status})

                except req.exceptions.Timeout:
                    # Maybe set up for a retry, or continue in a retry loop
                    return jsonify({"status": "SUCCESS", "message": error_parser.get('invalid', '_timeout')})
                except req.exceptions.TooManyRedirects:
                    # Tell the user their URL was bad and try a different one
                    return jsonify({"status": "SUCCESS", "message": error_parser.get('invalid', '_too_many_redirects')})
                except req.exceptions.RequestException:
                    # catastrophic error. bail.
                    return jsonify({"status": "SUCCESS", "message": error_parser.get('invalid', '_request_exception')})
                except req.exceptions.HTTPError:
                    return jsonify({"status": "SUCCESS", "message": error_parser.get('invalid', '_http_error')})
    else:
        return jsonify({"status": "SUCCESS", "message": error_parser.get('invalid', '_details_missing')})


@app.route('/api/experian/report/<int:id>/<type>', methods=['GET'])
def _get_experian_report(id=None, type=None):
    if id is not None and type is not None:
        return _get_result(query_parser.get('experian', '_get_experian_report_by_id').format(699, type))
    else:
        return jsonify({"status": "SUCCESS", "message": error_parser.get('invalid', '_details_missing')})


@app.route('/api/states', methods=['GET'])
def _get_states():
    _query = "SELECT id, name from m_states WHERE is_active is TRUE"
    return _get_result(_query)


@app.route('/api/regions', methods=['GET'])
def _get_regions():
    _query = "SELECT distinct RTRIM(name) as region, id from m_regions"
    return _get_result(_query)


@app.route('/api/regions/<int:state_id>', methods=['GET'])
def _get_regions_by_state(state_id=None):
    _query = "SELECT id, name from m_regions where state_id={}".format(state_id)
    return _get_result(_query)


@app.route('/api/regions/all', methods=['GET'])
def _get_all_regions():
    _query = "SELECT RTRIM(name) as name,id as region from m_regions"
    return _get_result(_query)


@app.route('/api/user/<int:user_id>/map/region', methods=['POST', 'DELETE', 'GET'])
def _map_region_by_id(user_id=None):
    if request.method == 'GET':
        return jsonify({'status': 'SUCCESS', 'data': _get_user_regions(user_id)})
    else:
        return _map_region(user_id)


@app.route('/api/user/map/region', methods=['POST', 'DELETE'])
@jwt_required
def _map_region(_user_id=None):
    if _user_id is None:
        logger.debug("User id:{}".format(_user_id))
        _user_id = get_jwt_identity()
    data = request.get_json()
    _region_data = data.get('regions')
    if request.method == 'POST':
        _query = "INSERT into t_users_region_map(user_id,region_id) values (%s, %s)"
        _insert_region_data = list()
        for _region in _region_data:
            _value = (_user_id, _region)
            _insert_region_data.append(_value)
        mydb._bulk_insert(_query, _insert_region_data)
        return jsonify({"status": "SUCCESS", "message": "Regions updated for the user."})
    elif request.method == 'DELETE':
        logger.debug(len(_region_data))
        _query = "DELETE FROM t_users_region_map WHERE user_id = {0} AND region_id IN {1}".format(_user_id,
                                                                                                  tuple(
                                                                                                      _region_data) if (
                                                                                                          len(
                                                                                                              _region_data) > 1) else "(" + str(
                                                                                                      _region_data[
                                                                                                          0]) + ")")
        _result = mydb.run_query(mydb.DELETE, _query)
        if _result > 0:
            return jsonify(
                {"status": "SUCCESS", "message": "Regions has been unmapped successfully.", "result": _result})
        else:
            return jsonify(
                {"status": "ERROR", "message": "Requested regions were not found mapped to the user",
                 "result": _result})


@app.route('/api/loan/report/dealership/<dealership_id>', methods=['GET'])
def _get_loan_due_report(dealership_id=None):
    if dealership_id is not None:
        data = dict()
        _sanctioned_loan_amount = mydb.run_query(mydb.SELECT,
                                                 "SELECT amount_disbursed from t_dealership_loans WHERE dealership_id={}".format(
                                                     dealership_id))
        query_customer = query_parser.get('dealership', '_get_customer_details').format(dealership_id)
        query_overdue = query_parser.get('dealership', '_get_loan_overdue').format(dealership_id)
        query_due = query_parser.get('dealership', '_get_loan_due').format(dealership_id)
        if _sanctioned_loan_amount:
            data.update({"sanctioned_loan_amount": (_sanctioned_loan_amount[0].get('amount_disbursed'), 0)})
        data.update({"cust_details": _get_data_from_lms(query_customer)})
        data.update({"overdue": _get_data_from_lms(query_overdue)})
        data.update({"due": _get_data_from_lms(query_due)})
        return jsonify({'status': 'SUCCESS', 'data': data})
    else:
        return jsonify({'status': 'ERROR', 'message': "Customer code (dealership_id) is empty. Contact administrator."})


def _email_report(reports):
    msg = dict()
    attachments = list()
    file_names = list()
    _cur_date = datetime.date.today()
    msg.update({"subject": "Petromoney loan reports for ( {}-{}-{} ) - Reg.".format(_cur_date.day, _cur_date.month,
                                                                                    _cur_date.year)})
    msg.update({
        "body": "Hi Team,\nGreetings from Petromoney!\nPlease find attached Loan reports (Due/Overdue) of {}-{}-{} for your perusal.\nFor any discrepancies, please contact administrator.\nThanks\nPetromoney IT".format(
            _cur_date.day, _cur_date.month, _cur_date.year)})
    msg.update({"recipients": report_recipients})
    for report_key in reports.keys():
        df = pd.DataFrame(reports[report_key])
        attachment_path = './reports/' + report_key + '.csv'
        attachments.append(attachment_path)
        file_names.append(report_key + '.csv')
        df.to_csv(index=False, path_or_buf=attachment_path)
    return _send_mail(msg, attachment=attachments, file_name=file_names, file_type="text/csv")


@app.route('/api/loan/report/<int:_is_report>', methods=['POST'])
@app.route('/api/loan/report', methods=['GET'])
@jwt_required
def _get_loan_report(_is_report=None):
    data = dict()
    current_user = get_jwt_identity()
    logger.info("Current user for report: {}".format(current_user))
    query = (query_parser.get('users', '_get_user_details') + " AND u.id = {}").format(current_user)
    logger.info(query)
    current_user_details = mydb.run_query(mydb.SELECT, query)
    query_due = ""
    query_overdue = ""
    if (request.args.get('region') == 'ALL'):
        query_due = query_parser.get('dealership', '_get_all_loan_dues')
        query_overdue = query_parser.get('dealership', '_get_all_loan_overdues')
    elif (current_user_details[0].get('role_id') == 13):
        return _get_loan_due_report((mydb.run_query(mydb.SELECT,
                                                    query_parser.get('dealers', '_get_dealership_id').format(
                                                        current_user_details[0].get('mobile'))))[0].get(
            'dealership_id'))
    else:
        _user_regions = mydb.run_query(mydb.SELECT,
                                       query_parser.get('users', '_get_user_regions').format(
                                           current_user))  # current_user
        region_list = ''
        if not _user_regions:
            return jsonify({'status': 'ERROR', 'message': "Regions not mapped to the User. Contact Administrator."})
        for region in _user_regions:
            region_list += "'{} Retail RO',".format(region.get('region_name'))
        region_list = region_list[:-1]
        query_due = query_parser.get('dealership', '_get_due_report_by_region').format(region_list)
        query_overdue = query_parser.get('dealership', '_get_overdue_report_by_region').format(region_list)
    data.update({"overdue": _get_data_from_lms(query_overdue)})
    data.update({"due": _get_data_from_lms(query_due)})

    if (_is_report == 1):
        _report_dict = dict()
        _report_dict.update({"due_report": data.get('due'), "overdue_report": data.get('overdue')})
        _email_status = _email_report(_report_dict).get_json()
        logger.debug(_email_status)
        if _email_status.get('delivery') == True:
            return jsonify({'status': 'SUCCESS', 'message': _email_status.get('message')})
        else:
            return jsonify({'status': 'ERROR', 'message': _email_status.get('message')})

    logger.info("Current data for loan report returned.")
    return jsonify({'status': 'SUCCESS', 'data': data})


@app.route('/api/business/products', methods=['GET'])
@jwt_required
def _loan_products():
    if request.method == 'GET':
        _query = query_parser.get('business', '_get_loan_products')
        return _get_result(_query)


@app.route('/api/business/loanbook', methods=['GET'])
# @jwt_required
def _loan_book():
    if request.method == 'GET':
        return jsonify({'status': 'SUCCESS', 'data': _get_data_from_lms(query_parser.get('mdm', '_get_loan_book'))})


@app.route('/api/metrics/loan/stats', methods=['GET'])
@jwt_required
def _loan_stats():
    if request.method == 'GET':
        query = query_parser.get('mdm', '_get_loans_count_by_status')
        return _get_result(query)


@app.route('/api/business/metrics/ls1', methods=['GET'])
# @jwt_required
def _business_metrics_lsone():
    if request.method == 'GET':
        return jsonify(
            {'status': 'SUCCESS', 'data': _get_data_from_lms(query_parser.get('mdm', '_get_metrics_v_lsone'))})


@app.route('/api/business/metrics/ls2', methods=['GET'])
# @jwt_required
def _business_metrics_lstwo():
    if request.method == 'GET':
        return jsonify(
            {'status': 'SUCCESS', 'data': _get_data_from_lms(query_parser.get('mdm', '_get_metrics_v_lstwo'))})


@app.route('/api/business/property', methods=['GET', 'POST'])
def _get_business_property():
    if request.method == 'GET':
        return _get_result(query_parser.get('business_property', '_get_business_property'))
    elif request.method == 'POST':
        data = request.get_json()
        return _is_rows_affected(
            (query_parser.get('business_property', '_post_business_property')).format(data.get('property_name')))
    else:
        return jsonify(error_parser.get('invalid', '_invalid_request')), 200


@app.route('/api/business/types', methods=['GET', 'POST'])
def _get_business_types():
    if request.method == 'GET':
        return _get_result(query_parser.get('business_type', '_get_business_type'))
    elif request.method == 'POST':
        data = request.get_json()
        return _is_rows_affected(
            (query_parser.get('business_type', '_post_business_type')).format(data.get('type_name')))
    else:
        return jsonify(error_parser.get('invalid', '_invalid_request'), 200)


@app.route('/api/app/modules', methods=['GET'])
@jwt_required
def _app_modules():
    current_user = get_jwt_identity()
    user_role = mydb.run_query(mydb.SELECT, query_parser.get('users', '_get_user_role').format(current_user))
    logger.debug(user_role)
    if request.method == 'GET':
        if (user_role[0].get('role_id') == 13):
            return _get_result(query_parser.get('general', '_get_app_modules_dealer'))
        else:
            return _get_result(query_parser.get('general', '_get_app_modules'))


@app.route('/api/business/banks', methods=['GET', 'POST'])
def _get_business_bank_details():
    if request.method == 'GET':
        return _get_result(query_parser.get('business_banks', '_get_business_banks'))
    elif request.method == 'POST':
        data = request.get_json()
        return _is_rows_affected(
            (query_parser.get('business_banks', '_post_business_banks')).format(data.get('type_name')))
    else:
        return jsonify(error_parser.get('invalid', '_invalid_request'), 200)


@app.route('/api/submit/dealership/<dealership_id>', methods=['POST'])
@jwt_required
def _submit_application(dealership_id=None):
    if dealership_id is not None:
        # TODO
        # Get loan_id in request while submitting the application from mobile app
        request_data = request.get_json()
        files_to_attach = list()
        files_name = list()
        logger.debug('request data in submit application --> {}'.format(request_data))
        loan_id = request_data.get('loan_id')
        profile_state = _check_profile_completion_status(dealership_id).get_json()
        logger.debug(profile_state)
        logger.debug(profile_state.get('completion_status'))
        if (profile_state.get('completion_status')):
            msg = dict()
            data = dict()
            _target_file_path = app.config['TARGET_FILE_PATH'] + "/{}/".format(str(dealership_id))
            _file_name = "{}_loan_application.html".format(dealership_id)
            file_to_attach = _target_file_path + "{}_loan_application.pdf".format(dealership_id)
            ds_query = query_parser.get('dealership', '_get_dealership_details_by_id').format(dealership_id)
            dl_query = query_parser.get('dealers', '_get_dealer_details_by_id').format(dealership_id)
            dbd_query = query_parser.get('dealership', '_get_bank_details').format(dealership_id)
            bd_query = query_parser.get('dealership', '_get_dealership_business_age_by_id').format(dealership_id)
            dealership_details = mydb.run_query(mydb.SELECT, ds_query)
            dealer_details = mydb.run_query(mydb.SELECT, dl_query)
            bank_details = mydb.run_query(mydb.SELECT, dbd_query)
            business_details = mydb.run_query(mydb.SELECT, bd_query)
            logger.debug("Loan application submission......")

            '''   Enable only for Debug purposes    '''
            '''
            logger.debug(dealership_details)
            logger.debug(dealer_details)
            logger.debug(bank_details)
            logger.debug(business_details)
            '''

            if dealership_details and dealership_details is not None:
                data.update(dealership_details[0])
            if dealer_details and dealer_details is not None:
                data.update(dealer_details[0])
            if bank_details and bank_details is not None:
                data.update(bank_details[0])
            if business_details and business_details is not None:
                data.update(business_details[0])
            _application_form = _find_and_replace(app.config['APPLICATION_TEMPLATE'], _target_file_path, _file_name,
                                                  data)
            if _application_form is not None:
                with open(_application_form) as fin:
                    pdfkit.from_file(fin, os.path.join(file_to_attach), options=options, configuration=_wkhtml_config)
                    logger.debug("Target file is in path: {}".format(file_to_attach))
            msg.update({"subject": "Application form for Loan request by {} ({})".format(
                dealership_details[0].get('name'), dealership_id)})
            msg.update({
                "body": "Dear Sir/Madam,\nGreetings!\n\nA new application for loan has been submitted by {} (dealership ID: {})".format(
                    dealership_details[0].get('name'), dealership_id)})
            fo_rm_email_list = mydb.run_query(mydb.SELECT, query_parser.get('users','_get_fo_rm_email_by_dealership_id').format(dealership_id))
            for email_id in fo_rm_email_list:
                recipients.append(email_id.get('email'))
            logger.debug("Email list:{}".format(recipients))
            msg.update({"recipients": recipients})
            files_to_attach.append(file_to_attach)
            files_name.append("{}_loan_application.pdf".format(dealership_id))
            email_status = _send_mail(msg, attachment=files_to_attach, file_name=files_name)
            logger.debug("Email send request status: {}".format(email_status.get_json()))
            query = query_parser.get('dealership', '_update_loan_status_submit').format("submitted", dealership_id)
            result = mydb.run_query(mydb.UPDATE, query)
            logger.info("Loan status update result: {}".format(result))
            return jsonify({"status": "SUCCESS", "message": "Form submitted successfully",
                            "email_status": email_status.get_json(), "update_result": result})
        else:
            return jsonify({"status": "ERROR", "data": profile_state,
                            "message": "Please check if Dealership, Dealers and Loan request is filled."})
    return jsonify(error_parser.get('invalid', '_id_details_missing'), 200)


@app.route('/api/dealership/<int:dealership_id>/loan/<int:loan_id>/resubmit', methods=['POST'])
@jwt_required
def _resubmit_application(dealership_id=None, loan_id=None):
    if all([dealership_id, loan_id]):
        query = query_parser.get('dealership', '_update_loan_status').format("submitted", loan_id)
        msg = dict()
        msg.update({"recipients": recipients})
        msg.update({
            "subject": "Loan application with loan number({1}) of dealership({0}) is re-submitted for processing - Reg.".format(
                dealership_id, loan_id)})
        msg.update({
            "body": "Dear Team,\n"
                    "The following loan application has been resubmitted to submitted queue for re-processing.\n\n"
                    "https://mdm.petromoney.in/#/dealership/{0}".format(dealership_id)})
        result = mydb.run_query(mydb.UPDATE, query)
        logger.info("Result: {}".format(result))
        if result is not None:
            email_status = _send_mail(msg)
            logger.info("Re-submission request email status: {}".format(email_status))
        return jsonify({"status": "SUCCESS", "message": "Loan application re-submitted successfully",
                        "email_status": email_status.get_json(), "result": result})
    else:
        return jsonify({"status": "ERROR", "message": error_parser.get('invalid', '_incomplete_details')})


@app.route('/api/dealership/<int:dealership_id>/loan/<int:loan_id>/approval', methods=['POST', 'DELETE'])
@jwt_required
def _send_application_approval(dealership_id=None, loan_id=None):
    if dealership_id is not None:
        if request.method == 'POST':
            data = request.get_json()
            data.update({'dealership_id': dealership_id, 'loan_id': loan_id, 'last_modified_by': get_jwt_identity()})
            msg = dict()
            email_status = ""
            fo_rm_email_list = mydb.run_query(mydb.SELECT, query_parser.get('users','_get_fo_rm_email_by_dealership_id').format(dealership_id))
            for email_id in fo_rm_email_list:
                recipients.append(email_id.get('email'))
            msg.update({"recipients": recipients})
            now = datetime.datetime.now()
            _loan_status = mydb.run_query(mydb.SELECT,
                                          query_parser.get('dealership', '_get_loan_status').format(loan_id))
            query = query_parser.get('dealership', '_update_loan_status').format(data.get('status'), loan_id)
            col_list = ['loan_id', 'dealership_id', 'recommendation_remarks', 'approval_remarks',
                        'disbursement_recommendation_remarks', 'disbursement_approval_remarks', 'disbursement_remarks']
            query_remarks = mydb._gen_upsert_query("t_dealership_loan_remarks", col_list, data)

            if (data.get('status') == "loan_approval" and (_loan_status[0].get('status')).lower() == "submitted"):
                msg.update({"subject": "Loan approval request for dealership({0}) with loan number:{1}".format(
                    dealership_id, loan_id)})
                msg.update({
                    "body": "Dear Sir,\nGreetings!\n\nThe following loan application awaits your approval."
                            "\n\n Please, approve/reject the request from the below url: "
                            "\n\n http://mdm.petromoney.in/#/dealership/{0}".format(dealership_id)})
                result = mydb.run_query(mydb.UPDATE, query)
                remarks_result = mydb.run_query(mydb.INSERT, query_remarks)
                logger.info("Result: {}".format(result))
                logger.info("Remarks result: {}".format(remarks_result))
                if result is not None:
                    email_status = _send_mail(msg)
                    logger.info("Approval request email status: {}".format(email_status))
                return jsonify({"status": "SUCCESS", "message": "Approval request submitted successfully",
                                "email_status": email_status.get_json(), "result": result})

            elif (data.get('status') == "approved" and (_loan_status[0].get('status')).lower() == "loan_approval"):
                data.update({'loan_approved_rejected_date': now.strftime('%Y-%m-%d %H:%M:%S')})
                msg.update({"subject": "Loan approval request approved for dealership({0}) with loan number:{1}".format(
                    dealership_id, loan_id)})
                msg.update({
                    "body": "Dear Team,\n\nThe following loan application has been granted initial level approval.\n\n Please proceed with document collection for further process."
                            "\n\n Please, submit the documents and additional details for the loan application from the below url: \n\n http://mdm.petromoney.in/#/dealership/{0}".format(
                        dealership_id)})
                cols = mydb._get_columns("t_dealership_loans")
                logger.info(data)
                loan_update_query = mydb._gen_update_query("t_dealership_loans", cols, data)
                logger.info(loan_update_query + " WHERE id={}".format(loan_id))
                result = mydb.run_query(mydb.UPDATE, loan_update_query + " WHERE id={}".format(loan_id))
                remarks_result = mydb.run_query(mydb.INSERT, query_remarks)
                logger.info("Result: {}".format(result))
                logger.info("Remarks_result: {}".format(remarks_result))
                if result is not None:
                    email_status = _send_mail(msg)
                    logger.info("Loan approval email status: {}".format(email_status))
                return jsonify({"status": "SUCCESS", "message": "Loan request approved successfully",
                                "email_status": email_status.get_json(), "result": result})

            elif (data.get('status') == "disbursement_approval" and (
                    _loan_status[0].get('status')).lower() == "approved"):
                msg.update({"subject": "Disbursement approval request for dealership({0}) with loan number:{1}".format(
                    dealership_id, loan_id)})
                msg.update({
                    "body": "Dear Sir,\nGreetings!\n\nThe following loan application awaits your approval for disbursement."
                            "\n\n Please, approve/reject the request from the below url: \n\n http://mdm.petromoney.in/#/dealership/{0}".format(
                        dealership_id)})
                result = mydb.run_query(mydb.UPDATE, query)
                remarks_result = mydb.run_query(mydb.INSERT, query_remarks)
                logger.info("Result: {}".format(result))
                logger.info("Remarks_result: {}".format(remarks_result))
                if result is not None:
                    email_status = _send_mail(msg)
                    logger.info("disbursement request email status: {}".format(email_status))
                return jsonify({"status": "SUCCESS", "message": "Approval request submitted successfully",
                                "email_status": email_status.get_json(), "result": result})

            elif (data.get('status') == "disbursement_approved" and (
                    _loan_status[0].get('status')).lower() == "disbursement_approval"):
                data.update({'loan_disbursement_approved_rejected_date': now.strftime('%Y-%m-%d %H:%M:%S')})
                msg.update({
                    "subject": "Loan disbursement approval request approved for dealership({0}) with loan number:{1}".format(
                        dealership_id, loan_id)})
                msg.update({
                    "body": "Dear Team,\n\nThe following loan application has been approved for disbursement."
                            "\n\n Please proceed with the further process of disbursement."
                            "\n\n Please, view the loan application from the below url: "
                            "\n\n http://mdm.petromoney.in/#/dealership/{0}".format(dealership_id)})
                cols = mydb._get_columns("t_dealership_loans")
                loan_update_query = mydb._gen_update_query("t_dealership_loans", cols, data)
                logger.info(loan_update_query + " WHERE id={}".format(loan_id))
                result = mydb.run_query(mydb.UPDATE, loan_update_query + " WHERE id={}".format(loan_id))
                remarks_result = mydb.run_query(mydb.INSERT, query_remarks)
                logger.info("Result: {}".format(result))
                logger.info("Remarks_result: {}".format(remarks_result))
                if result is not None:
                    email_status = _send_mail(msg)
                    logger.info("Loan disbursement approval email status : {}".format(email_status))
                return jsonify({"status": "SUCCESS", "message": "Loan disbursement request approved successfully",
                                "email_status": email_status.get_json(), "result": result})

            elif (data.get('status') == "disbursed" and (
                    _loan_status[0].get('status')).lower() == "disbursement_approved"):
                data.update({'loan_disbursed_date': now.strftime('%Y-%m-%d %H:%M:%S')})
                msg.update({"subject": "Loan has been disbursed for dealership({0}) with loan number:{1}".format(
                    dealership_id, loan_id)})
                msg.update({
                    "body": "Dear Team,\n\nThe following loan application has been disbursed.\n\n "
                            "\n\n Please, view the details for the loan application from the below url: "
                            "\n\n http://mdm.petromoney.in/#/dealership/{0}".format(dealership_id)})
                cols = mydb._get_columns("t_dealership_loans")
                disbursement_details_cols = ["loan_id", "applicant_code", "prospect_code", "disbursement_date",
                                             "amount", "disbursement_status"]
                logger.info("Request data {}\n:".format(data))
                loan_update_query = mydb._gen_update_query("t_dealership_loans", cols, data)
                disbursement_details_query = mydb._gen_insert_query_exclude_cols(
                    "t_dealership_loan_disbursement_details", disbursement_details_cols, data)
                result = mydb.run_query(mydb.UPDATE, loan_update_query + " WHERE id={}".format(loan_id))
                remarks_result = mydb.run_query(mydb.INSERT, query_remarks)
                disbursement_details_result = mydb.run_query(mydb.INSERT, disbursement_details_query)
                _map_disbursement_details(dealership_id, data)
                dealer_data = _get_result_as_dict("SELECT first_name, last_name, email, mobile, 13 as role_id FROM m_dealers WHERE dealership_id={}".format(dealership_id))[0]
                _dealer_user_account = _signup(dealer_data, otp=False)
                logger.info("Result: {}".format(result))
                logger.info("Remarks result: {}".format(remarks_result))
                logger.info("Disbursement details result: {}".format(disbursement_details_result))
                logger.info("User account creation response: {}".format(_dealer_user_account))
                if result is not None:
                    email_status = _send_mail(msg)
                    logger.info("Loan disbursement email status: {}".format(email_status))
                return jsonify({"status": "SUCCESS", "message": "Loan has been disbursed successfully",
                                "email_status": email_status.get_json(), "result": result})

            elif data.get('status') == "rejected":
                data.update({'loan_approved_rejected_date': now.strftime('%Y-%m-%d %H:%M:%S')})
                msg.update({"subject": "Loan application of dealership({0}) with loan number:{1} is rejected.".format(
                    dealership_id, loan_id)})
                msg.update({
                    "body": "Dear Sir,\nGreetings!\n\nWe regret to inform that the following loan application is rejected."
                            "\n\n Kindly, follow the comments from the below link. "
                            "\n\n http://mdm.petromoney.in/#/dealership/{0}".format(dealership_id)})
                cols = mydb._get_columns("t_dealership_loans")
                loan_update_query = mydb._gen_update_query("t_dealership_loans", cols, data)
                result = mydb.run_query(mydb.UPDATE, loan_update_query + " WHERE id={}".format(loan_id))
                remarks_result = mydb.run_query(mydb.INSERT, query_remarks)
                logger.info("Result: {}".format(result))
                logger.info("Remarks result: {}".format(remarks_result))
                if result is not None:
                    email_status = _send_mail(msg)
                    logger.info("Loan request reject email status: {}".format(email_status))
                return jsonify({"status": "SUCCESS", "message": "Loan application has been rejected.",
                                "email_status": email_status.get_json(), "result": result})

            elif (data.get('status') == "disbursed" and (
                    _loan_status[0].get('status')).lower() == "disbursed"):
                msg.update({
                    "subject": "New installment has been disbursed for Loan for dealership({0}) with loan number:{1}".format(
                        dealership_id, loan_id)})
                msg.update({
                    "body": "Dear Team,\n\nA new installment has been disbursed for the following loan.\n\n "
                            "\n\n Please, view the details for the loan application from the below url: "
                            "\n\n http://mdm.petromoney.in/#/dealership/{0}".format(dealership_id)})
                disbursement_details_cols = ["loan_id", "applicant_code", "prospect_code", "disbursement_date",
                                             "amount", "disbursement_status"]
                logger.info("Request data {}\n:".format(data))
                disbursement_details_upsert_query = mydb._gen_upsert_query("t_dealership_loan_disbursement_details",
                                                                           disbursement_details_cols, data)
                details_upsert_result = mydb.run_query(mydb.INSERT, disbursement_details_upsert_query)
                logger.info("Disbursement details result: {}".format(details_upsert_result))
                _map_disbursement_details(dealership_id, data)
                if details_upsert_result is not None:
                    email_status = _send_mail(msg)
                    logger.info("Loan disbursement email status: {0}".format(email_status))
                return jsonify({"status": "SUCCESS", "message": "Loan has been disbursed successfully",
                                "email_status": email_status.get_json(), "result": details_upsert_result})

        elif request.method == "DELETE":
            data = request.get_json()
            _applicant_code = data.get('applicant_code')
            _prospect_code = data.get('prospect_code')
            if all([_applicant_code,_prospect_code]):
                _hide_details = mydb.run_query(mydb.DELETE, query_parser.get('dealership', '_remove_loan_disbursement_details').format(loan_id, _applicant_code, _prospect_code))
                return jsonify({"status": "SUCCESS", "message": "Loan disbursement detail removed."})
            else:
                return jsonify({"status": "ERROR", "message": error_parser.get('invalid', '_incomplete_details')})
        else:
            return jsonify(
                {"status": "ERROR", "message": error_parser.get('invalid', '_incorrect_approval_request')})


def _map_disbursement_details(dealership_id, data):
    logger.debug("updating disbursement details to lms......")
    dealer_data = _get_result_as_dict("SELECT md.name, r.name as region_name, md.omc FROM m_dealership md "
                                      "LEFT JOIN m_regions r ON md.region=r.id WHERE md.id={0}".format(dealership_id))[
        0]
    logger.debug(dealer_data)
    stmt = query_parser.get('lms', '_add_disbursement_details').format(data.get('prospect_code'),
                                                                       data.get('applicant_code'),
                                                                       dealer_data.get('name'),
                                                                       data.get('disbursement_date'),
                                                                       data.get('amount'),
                                                                       datetime.datetime.strptime(
                                                                           data.get('disbursement_date'),
                                                                           "%Y/%m/%d").strftime("%b").upper(),
                                                                       dealership_id,
                                                                       dealer_data.get('region_name'),
                                                                       dealer_data.get('region_name'),
                                                                       dealer_data.get('omc'))
    return _execute_lms_query(stmt)


@app.route('/api/checklist/<int:dealership_id>', methods=['GET'])
@app.route('/api/checklist/<int:dealership_id>/doc/<int:doc_id>', methods=['GET', 'POST'])
@app.route('/api/checklist/<int:dealership_id>/doc/<int:doc_id>/<int:id>', methods=['POST'])
@app.route('/api/checklist/<int:dealership_id>/<int:dealer_id>', methods=['GET'])
@app.route('/api/checklist/<int:dealership_id>/<int:dealer_id>/<int:doc_id>', methods=['GET', 'POST'])
@app.route('/api/checklist/<int:dealership_id>/<int:dealer_id>/<int:doc_id>/<int:id>', methods=['POST'])
def _get_document_checklist(dealership_id=None, doc_id=None, dealer_id=None, id=None):
    if dealership_id is not None:
        if dealer_id is not None:
            if request.method == 'GET':
                return _get_result(
                    query_parser.get('checklist', '_get_documents_checklist_status_dealer_id').format(dealership_id,
                                                                                                      dealer_id))
        else:
            if request.method == 'GET':
                query = query_parser.get('checklist', '_get_documents_checklist_status').format(dealership_id)
                res_data = mydb.run_query(mydb.SELECT, query)
                _formatted_file_data = _format_files_data(res_data, 'doc_id')
                return jsonify({'status': 'SUCCESS', 'data': _formatted_file_data})

        if request.method == 'POST':
            logger.info("Request object {}".format(request))
            data = dict()
            '''Marking for updation, need to remove status check and do doc check using the _s3_file_url list length'''
            _status = False
            _s3_file_url = list()
            _split_file_urls = list()

            if (request.files is not None):
                logger.info("Files in request")
                logger.info(request.files)
                try:
                    for file in request.files:
                        _status, _url = _file_upload(request.files.getlist(file), dealership_id, dealer_id, doc_id)
                        if _status:
                            if len(_url.split(" ")) >= 1:
                                _split_file_urls = _url.split(" ")
                                _s3_file_url.append(_split_file_urls)
                            else:
                                _s3_file_url.append(_url)
                            if id is None:
                                for _split_url in _split_file_urls:
                                    if dealer_id is None:
                                        _query = query_parser.get('checklist', '_post_documents_checklist_status') \
                                            .format(dealership_id, 0, doc_id, 1, _split_url)
                                    else:
                                        _query = query_parser.get('checklist', '_post_documents_checklist_status') \
                                            .format(dealership_id, dealer_id, doc_id, 1, _split_url)
                                    _query_status = _execute_query(mydb.INSERT, _query)
                                    logger.debug("New Document uploaded for dealership-id %s and the response is %s",
                                                 dealership_id, _query_status.get_json())
                                    data.update(
                                        {'dealership_id': dealership_id, 'dealer_id': dealer_id, 'doc_id': doc_id,
                                         'url': _s3_file_url})
                            else:
                                _query = query_parser.get('checklist', '_update_documents_checklist_status').format(1,
                                                                                                                    _url,
                                                                                                                    id)
                                _query_status = _execute_query(mydb.UPDATE, _query)
                                logger.debug("Document checklist updated for dealership-id %s and the response is %s",
                                             dealership_id, _query_status.get_json())
                                data.update(
                                    {'dealership_id': dealership_id, 'dealer_id': dealer_id, 'doc_id': doc_id,
                                     'url': _s3_file_url})
                        else:
                            return jsonify({'status': 'SUCCESS', 'message': "No files updated."})
                    return jsonify(
                        {'status': 'SUCCESS', 'message': "File uploaded successfully", 'data': data})
                except OSError as e:
                    logger.debug('Error: {}'.format(e))
                    return jsonify({"status": "FAILURE",
                                    "message": "Details could not be updated as file can't be uploaded. "
                                               "Please email the document to sales@petromoney.co.in"})
            else:
                return jsonify({"status": "FAILURE",
                                "message": "No files in the request to upload."})
    else:
        return jsonify({'status': 'ERROR', 'message': error_parser.get('invalid', '_invalid_request')})


@app.route('/api/checklist/<int:dealership_id>', methods=['DELETE'])
@jwt_required
def _remove_documents(dealership_id=None):
    data = request.get_json()
    _ids = data.get("id")
    logger.debug("Delete documents with following ids:{}".format(_ids))
    _ids = tuple(_ids) if len(_ids) > 1 else "(" + str(_ids[0]) + ")"
    logger.debug("Document id(s) to disable: {}".format(_ids))
    _result = mydb.run_query(mydb.UPDATE,
                             query_parser.get('checklist', '_disable_documents').format(dealership_id, _ids))
    return jsonify({'status': 'SUCCESS', 'message': 'Document(s) removed successfully.', 'data': _result})


@app.route('/api/checklist/<int:dealership_id>/doc/<int:id>', methods=['DELETE'])
@jwt_required
def _remove_document(dealership_id=None, id=None):
    _result = mydb.run_query(mydb.UPDATE, query_parser.get('checklist', '_disable_document').format(dealership_id, id))
    return jsonify({'status': 'SUCCESS', 'message': 'Document removed successfully.', 'data': _result})


@app.route('/api/passbook/dealership/<int:dealership_id>', methods=['GET'])
def _get_dealership_passbook(dealership_id=None):
    if dealership_id is not None:
        data = _get_data_from_lms(query_parser.get('dealership', '_get_passbook').format(dealership_id))
        return jsonify({'status': 'SUCCESS', 'data': data})
    else:
        return jsonify(error_parser.get('invalid', '_id_details_missing')), 200


@app.route('/api/sales/dealership/<int:dealership_id>', methods=['GET'])
def _get_sales_history(dealership_id=None):
    if dealership_id is not None:
        return _get_result(query_parser.get('dealership', '_get_sales_history').format(dealership_id))
    else:
        return jsonify(error_parser.get('invalid', '_id_details_missing')), 200


@app.route('/api/dealership/<int:dealership_id>/salesdata', methods=['GET', 'POST', 'DELETE'])
@jwt_required
def _get_sales_data(dealership_id=None):
    if dealership_id is not None:
        if request.method == 'POST':
            data = request.get_json()
            logger.debug(data)
            ms = int(data.get('ms'))
            hsd = int(data.get('hsd'))
            ms_rs = ms * 1000 * 75
            hsd_rs = hsd * 1000 * 75
            ms_gross = ms * 1000 * 3
            hsd_gross = hsd * 1000 * 2
            data.update({"dealership_id": dealership_id, "last_modified_by": get_jwt_identity(), "ms_rs": ms_rs,
                         "hsd_rs": hsd_rs, "ms_gross": ms_gross, "hsd_gross": hsd_gross})
            col_list = mydb._get_columns("t_dealership_sales_data")
            query = mydb._gen_upsert_query("t_dealership_sales_data", col_list, data)
            return _is_rows_affected(query)
        elif request.method == 'GET':
            return _get_result(query_parser.get('dealership', '_get_sales_data').format(dealership_id))
        elif request.methods == 'DELETE':
            data = request.get_json()
            return _get_result(
                query_parser.get('dealership', '_remove_sales_data').format(dealership_id, data.get('from_year')))
    else:
        return jsonify(error_parser.get('invalid', '_id_details_missing')), 200


@app.route('/api/dealership/<int:dealership_id>/financials', methods=["GET", "POST"])
@jwt_required
def _get_financials(dealership_id=None):
    if dealership_id is not None:
        if request.method == 'POST':
            data = request.get_json()
            from_year = 2019 if data.get('type') == "latest_fy" else 2018
            data.update({"dealership_id": dealership_id, "last_modified_by": get_jwt_identity(),
                         "from_year": from_year, "to_year": from_year + 1})
            '''Net profit percentage of previous financial year'''
            _prev_yr_np_per = _get_prev_year_np(dealership_id, from_year)
            '''Net profit percentage of current financial year'''
            _cur_yr_np_per = 0
            if all([data.get('net_profit'), data.get('turnover')]):
                _cur_yr_np_per = int(data.get('net_profit')) / int(data.get('turnover'))
            '''Net profit change in percentage'''
            _np_percentage_change = (int(_cur_yr_np_per) - int(_prev_yr_np_per)) / int(_prev_yr_np_per) if int(
                _prev_yr_np_per) > 0 else 0
            '''Compute Leverage (No of Times)'''
            _leverage = 0
            _asset_networth_sum = int(data.get('assets_value', 0)) + int(data.get('networth', 0))
            if int(data.get('loan_os', 0)) > 0 and _asset_networth_sum > 0:
                _leverage = int(data.get('loan_os', 0)) / _asset_networth_sum
            data.update({"net_profit_percentage": _cur_yr_np_per,
                         "net_profit_change_percentage": _np_percentage_change,
                         "leverage": _leverage,
                         "change_in_profit_over_sales": _np_percentage_change})
            col_list = mydb._get_columns("t_dealership_financials")
            query = mydb._gen_upsert_query("t_dealership_financials", col_list, data)
            return _is_rows_affected(query)
        elif request.method == 'GET':
            return _get_result(query_parser.get('dealership', '_get_dealership_financials').format(dealership_id))
    else:
        return jsonify(error_parser.get('invalid', '_id_details_missing')), 200


@app.route('/api/transport/owners', methods=['GET'])
@app.route('/api/transport/owner/<int:t_owner_id>', methods=['GET'])
@jwt_required
def _get_transport_owners(t_owner_id=None):
    if t_owner_id:
        return _get_result(query_parser.get('transporters', '_get_transport_owner_by_id').format(t_owner_id))
    else:
        return _get_result(query_parser.get('transporters', '_get_transport_owners').format(get_jwt_identity()))


@app.route('/api/transport/owners/<int:dealership_id>', methods=['GET'])
@jwt_required
def _get_transport_owners_by_dealership_id(dealership_id=None):
    if dealership_id:
        return _get_result(query_parser.get('transporters', '_get_transport_owners_by_dealership_id').format(dealership_id))


@app.route('/api/transport/owner', methods=['POST'])
@app.route('/api/transport/owner/<int:t_owner_id>', methods=['POST'])
@jwt_required
def _transport_owners(t_owner_id=None):
    data = dict(request.form)
    data.update({"last_modified_by": get_jwt_identity()})
    _col_list = mydb._get_columns('t_transport_owners')
    if t_owner_id:
        if bool(request.files):
            _file_data = _upload_kyc_docs(request.files, t_owner_id)
            if _file_data:
                data.update(_file_data)
        _update_query = mydb._gen_update_query("t_transport_owners", _col_list, data)
        return _is_rows_affected(_update_query + " WHERE t_owner_id={}".format(t_owner_id))
    else:
        _add_query = mydb._gen_insert_query_exclude_cols("t_transport_owners", _col_list, data)
        affected_rows, t_owner_id = mydb.run_query(mydb.INSERT, _add_query, row_insert_id=True)
        if bool(request.files):
            _file_data = _upload_kyc_docs(request.files, t_owner_id)
            if _file_data:
                data.update(_file_data)
        _update_query = mydb._gen_update_query("t_transport_owners", _col_list, data)
        mydb.run_query(mydb.UPDATE, _update_query + " WHERE t_owner_id={}".format(t_owner_id))
        return jsonify({'status': 'SUCCESS', 'message': 'Transport owner added successfully'})


def _upload_kyc_docs(user_files, t_owner_id=None, transporter_id=None):
    _file_data = dict()
    try:
        for file in user_files:
            logger.debug("File in dealers request object : {}".format(file))
            if (file == 'gst_file_url'):
                _status, gst_file_url = _transporter_file_upload(request.files.getlist(file), t_owner_id, transporter_id, doc_id=2)
                logger.debug("GST file url: %s", gst_file_url)
                _file_data.update({'gst_file_url': gst_file_url})
            if (file == 'pan_file_url'):
                _status, pan_file_url = _transporter_file_upload(request.files.getlist(file), t_owner_id, transporter_id, doc_id=2)
                logger.debug("pan file url: %s", pan_file_url)
                _file_data.update({'pan_file_url': pan_file_url})
            if (file == 'aadhar_f_file_url'):
                _status, aadhar_f_file_url = _transporter_file_upload(request.files.getlist(file), t_owner_id, transporter_id, doc_id=3)
                logger.debug("aadhar_f_file_url: %s", aadhar_f_file_url)
                _file_data.update({'aadhar_f_file_url': aadhar_f_file_url})
            if (file == 'aadhar_b_file_url'):
                _status, aadhar_b_file_url = _transporter_file_upload(request.files.getlist(file), t_owner_id, transporter_id, doc_id=3)
                logger.debug("aadhar_b_file_url: %s", aadhar_b_file_url)
                _file_data.update({'aadhar_b_file_url': aadhar_b_file_url})
            if (file == 'profile_image_url'):
                _status, profile_image_url = _transporter_file_upload(request.files.getlist(file), t_owner_id, transporter_id, doc_id=1)
                logger.debug("profile_image_url: %s", profile_image_url)
                _file_data.update({'profile_image_url': profile_image_url})
        return _file_data
    except Exception as e:
        logger.debug('File upload error because: {}'.format(e.message))
        return _file_data


@app.route('/api/transport/owner/<int:t_owner_id>', methods=['DELETE'])
@jwt_required
def _disable_transport_owners(t_owner_id=None):
    if t_owner_id:
        _col_list = mydb._get_columns('t_transport_owners')
        _query = mydb._gen_update_query('t_transport_owners', _col_list, {'status':0})
        return _execute_query(mydb.UPDATE, _query + " WHERE t_owner_id={}".format(t_owner_id))


@app.route('/api/transporters/exceptions', methods=['GET'])
@jwt_required
def _get_transporter_exceptions():
    return _get_result(query_parser.get('transporters', '_get_unmapped_transporters'))


@app.route('/api/transporterslist', methods=["GET"])
@jwt_required
def _get_transporters_list():
    return _get_result(query_parser.get('transporters', '_get_all_transporters'))


@app.route('/api/transporters/<int:transporter_id>', methods=["GET"])
@jwt_required
def _get_transporter_info(transporter_id=None):
    return _get_result(query_parser.get('transporters', '_get_transporter_info_by_id').format(transporter_id))


@app.route('/api/transporters/owner/<int:t_owner_id>', methods=["GET"])
@jwt_required
def _get_transporters(t_owner_id=None):
    return _get_result(query_parser.get('transporters', '_get_all_transports_by_owner_id').format(t_owner_id))


@app.route('/api/transporters', methods=["POST"])
@app.route('/api/transporters/<int:transporter_id>', methods=["POST"])
@jwt_required
def _transporters(transporter_id=None):
    if request.method == 'POST':
        data = dict(request.form)
        data.update({"last_modified_by":get_jwt_identity()})
        logger.debug("transport_owner_id {}".format(data.get('t_owner_id')))
        t_owner_id = data.get('t_owner_id')
        logger.debug("transport_owner_id {}".format(t_owner_id))
        logger.debug("transporter_id {}".format(transporter_id))
        _col_list = mydb._get_columns('t_transports')
        if transporter_id is None:
            _query = mydb._gen_insert_query_exclude_cols('t_transports',_col_list,data)
            affected_rows, transporter_id = mydb.run_query(mydb.INSERT, _query, row_insert_id=True)
        if bool(request.files):
            _file_data = _upload_kyc_docs(request.files, t_owner_id, transporter_id)
            if _file_data:
                data.update(_file_data)
        _query = mydb._gen_update_query("t_transports", _col_list, data)
        mydb.run_query(mydb.UPDATE, _query + " WHERE transporter_id={}".format(transporter_id))
        return jsonify({'status': 'SUCCESS', 'message': 'Transport added successfully'})


def _map_user_transport_owner(_map_data):
    _query = mydb._gen_insert_query('t_user_transport_owners_map', _map_data)
    return mydb.run_query(mydb.INSERT, _query)


@app.route('/api/transporters/<int:transporter_id>/vehicles', methods=["GET", "POST"])
@app.route('/api/transporters/<int:transporter_id>/vehicles/<int:vehicle_id>', methods=["POST"])
@jwt_required
def _get_transporters_vehicles(transporter_id=None, vehicle_id=None):
    if transporter_id is not None:
        if request.method == 'GET':
            return _get_result(query_parser.get('transporters', '_get_vehicles_info').format(transporter_id))
        elif request.method == 'POST':
            data = request.get_json()
            data.update({"transporter_id": transporter_id})
            if vehicle_id is not None:
                col_list = mydb._get_columns('t_transporter_vehicles')
                query = mydb._gen_update_query('t_transporter_vehicles', col_list, data)
                return _execute_query(mydb.UPDATE, query + " WHERE id = {0}".format(vehicle_id))
            else:
                query = mydb._gen_insert_query('t_transporter_vehicles', data)
                return _execute_query(mydb.INSERT, query)
    else:
        return jsonify(error_parser.get('invalid', '_id_details_missing')), 200


@app.route('/api/transporter/<int:transporter_id>/vehicle/<int:vehicle_id>/docs', methods=['GET', 'POST'])
@app.route('/api/transporter/<int:transporter_id>/vehicle/<int:vehicle_id>/docs/<int:id>', methods=['DELETE'])
@jwt_required
def _get_transporters_vehicles_documents(transporter_id=None, vehicle_id=None, id=None):
    if request.method == 'POST':
        data = dict(request.form)
        data.update({"last_modified_by": get_jwt_identity()})
        data.update({"transporter_id": transporter_id, "vehicle_id": vehicle_id})
        if request.files is not None:
            try:
                for file in request.files:
                    logger.debug("File in dealers request object : {}".format(file))
                    _status, file_path = _vehicle_file_upload(request.files.getlist(file), transporter_id, vehicle_id,
                                                              data.get('document_name'), doc_id=data.get('document_id'))
                    logger.debug("Vehicle supporting files url: %s", file_path)
                    data.update({'file_path': file_path})
                    col_list = mydb._get_columns('t_transporter_vehicle_document_checklist')
                    query = mydb._gen_insert_query_exclude_cols("t_transporter_vehicle_document_checklist", col_list, data)
                    logger.debug("Insert query for vehicle document upload: %s", query)
                    insert_response = _is_rows_affected(query)
                    logger.debug("Uploaded file urls updated in table and the response is: {}".format(
                        insert_response.get_json()))
                    return jsonify(
                        {"status": "success", "message": "File uploaded successfully", "file_url": file_path})
            except Exception as e:
                logger.debug('Error: {}'.format(e.message))
                return jsonify({"status": "ERROR",
                                "message": "Details could not be updated as file can't be uploaded. "
                                           "Please email the document to sales@petromoney.co.in"})
    elif request.method == 'DELETE':
        remove_query = query_parser.get('transporters', '_remove_vehicle_docs').format(0, id)
        return _execute_query(mydb.DELETE, remove_query)
    elif request.method == 'GET':
        return _get_result(query_parser.get('transporters', '_get_vehicle_docs').format(vehicle_id))


@app.route('/api/vehicle/<int:vehicle_id>/loan', methods=['POST', 'GET'])
@app.route('/api/vehicle/<int:vehicle_id>/loan/<int:loan_id>', methods=['POST', 'DELETE'])
@jwt_required
def _transporter_vehicle_loan(vehicle_id=None, loan_id=None):
    if request.method == 'POST':
        data = request.get_json()
        data.update({"last_modified_by": get_jwt_identity()})
        data.update({"vehicle_id": vehicle_id})
        _col_list = ['vehicle_id', 'credit_head_id', 'loan_amount', 'remarks', 'last_modified_by']
        if loan_id is not None:
            query = mydb._gen_update_query("t_transporter_loans", _col_list, data)
            logger.debug("Vehicle loan update query: %s", query + " WHERE id={}".format(loan_id))
            return _is_rows_affected(query + " WHERE id={}".format(loan_id))
        else:
            query = mydb._gen_insert_query("t_transporter_loans", data)
            logger.debug("Vehicle loan insert query: %s", query)
            return _is_rows_affected(query)
    elif request.method == "DELETE":
        if loan_id is not None:
            query = query_parser.get("transporters", "_remove_vehicle_loans").format(loan_id)
            return _execute_query(mydb.DELETE, query)
    elif request.method == "GET":
        return _get_result(query_parser.get('transporters', '_get_vehicle_loans').format(vehicle_id))


@app.route('/api/vehicle/loans', methods=['GET'])
@jwt_required
def _vehicle_list_with_loans():
    if request.method == 'GET':
        return _get_result(query_parser.get('transporters', '_get_vehicle_details_with_loan'))


@app.route('/api/vehicle/<int:vehicle_id>/service/<int:credit_head_id>/tracker/<int:loan_id>', methods=['GET', 'POST'])
@jwt_required
def _vehicle_loan_tracker(vehicle_id=None, credit_head_id=None, loan_id=None):
    if request.method == 'GET':
        data = dict()
        _service_steps = mydb.run_query(mydb.SELECT,
                                        query_parser.get('transporters', '_get_service_steps').format(credit_head_id))
        _service_tracking_details = mydb.run_query(mydb.SELECT, query_parser.get('transporters',
                                                                                 '_get_vehicle_service_status').format(
            vehicle_id, credit_head_id, loan_id))
        data.update(
            {"vehicle_id": vehicle_id, "credit_head_id": credit_head_id, "loan_id": loan_id, "steps": _service_steps,
             "tracking_details": _service_tracking_details})
        return jsonify({"status": "SUCCESS", "data": data})
    elif request.method == 'POST':
        data = request.get_json()
        data.update({"vehicle_id": vehicle_id, "option_id": credit_head_id, "loan_id": loan_id})
        _col_list = mydb._get_columns('vehicle_loan_tracker')
        _query = mydb._gen_upsert_query('vehicle_loan_tracker', _col_list, data)
        return _execute_query(mydb.INSERT, _query)


@app.route('/api/vehicle/<int:vehicle_id>/service/<int:loan_id>', methods=['POST'])
@jwt_required
def _vehicle_service_state(vehicle_id=None, loan_id=None):
    if request.method == 'POST':
        data = request.get_json()
        _status_info = mydb.run_query(mydb.SELECT,
                                      query_parser.get('transporters', '_get_service_status_info').format(vehicle_id,
                                                                                                          loan_id,
                                                                                                          data.get(
                                                                                                              "option_id"),
                                                                                                          data.get(
                                                                                                              "status_id")))
        if _status_info:
            _update_service = mydb.run_query(mydb.UPDATE,
                                             "UPDATE vehicle_loan_tracker SET details='{0}', last_modified_by='{1}' WHERE vehicle_id='{2}' AND loan_id='{3}' AND options_id='{4}' AND status_id='{5}'".format(
                                                 data.get("details"), data.get('user_id', 0), vehicle_id, loan_id,
                                                 data.get("option_id"), data.get("status_id")))
        else:
            _add_new_service = mydb.run_query(mydb.INSERT,
                                              "INSERT INTO vehicle_loan_tracker(vehicle_id, loan_id, option_id, status_id, details, last_modified_by) values('{0}', '{1}', '{2}', '{3}', '{4}', '{5}')".format(
                                                  vehicle_id, loan_id, data.get("option_id"), data.get("status_id"),
                                                  data.get("details"), data.get('user_id', 0)))

        return jsonify({"status": "SUCCESS", "message": "Service status updated"})


def _get_prev_year_np(id, year):
    query = "SELECT net_profit_percentage from t_dealership_financials " \
            "WHERE dealership_id={0} AND from_year={1} AND to_year={2}".format(id, year, int(year) + 1)
    result = mydb.run_query(mydb.SELECT, query)
    return 0 if _is_empty(result) else result[0].get('net_profit_percentage')


@app.route('/api/business/bookvalue', methods=['GET'])
def _get_book_value():
    data = _get_data_from_lms(query_parser.get('mdm', '_get_book_value'))
    logger.debug(data)
    return jsonify(
        {"status": "SUCCESS", "business_date": data[0].get('business_date'), "exposure": data[0].get('exposure')})


@app.route('/api/business/policy', methods=['POST', 'GET'])
@jwt_required
def _get_policy():
    if request.method == 'POST':
        data = request.get_json()
        data.update({'last_modified_by': get_jwt_identity()})
        return _execute_query(mydb.INSERT, mydb._gen_insert_query('t_policies', data))
    elif request.method == 'GET':
        if not request.args.get('type'):
            return _get_result(query_parser.get('policies', '_get_all_policies'))
        else:
            return _get_result(query_parser.get('policies', '_get_policy_by_type').format(request.args.get('type')))


@app.route('/api/customer/<int:customer_id>/recharge', methods=['POST'])
def _recharge_wallet(customer_id=None):
    if customer_id is not None:
        data = request.get_json()
        data.update({"remarks": "Wallet Recharge"})
        if (data.get('amount') == 0):
            return jsonify({"status": "ERROR", "message": "Please enter a valid amount to recharge."})
        else:
            result = _create_pay_order(customer_id, data)
            return jsonify({"status": "SUCCESS", "message": "Payment order created.", "data": result})


@app.route('/api/customer/<int:customer_id>/verify', methods=['POST'])
def _verify_payment_signature(customer_id=None):
    data = request.get_json()
    result = _rpay_client.utility.verify_payment_signature(data)
    logger.debug("Result of signature verification: {}".format(result))
    if result is None:
        _transaction_log_cols = ['rzp_payment_id', 'rzp_signature', 'status']
        _transaction_data = {"rzp_payment_id": data.get('razorpay_payment_id'),
                             "rzp_signature": data.get('razorpay_signature'),
                             "status": "verified"}
        _query = mydb._gen_update_query('t_razorpay_transaction_log', _transaction_log_cols, _transaction_data)
        logger.debug("Razorpay transaction log query: {}".format(_query))
        _log_result = _execute_query(mydb.UPDATE,
                                     _query + " WHERE customer_id={0} AND rzp_order_id='{1}'".format(customer_id,
                                                                                                     data.get(
                                                                                                         'order_id')))
        logger.debug("RZP Transaction log result for customer_id: {}".format(customer_id))
        _balance_query = "SELECT amount from t_razorpay_transaction_log where rzp_order_id='{}'".format(
            data.get('order_id'))
        _order_amount = mydb.run_query(mydb.SELECT, _balance_query)
        data.update({"amount": int(_order_amount[0].get('amount'))})
        _insert_customer_passbook(customer_id, data)
        return jsonify({"status": "SUCCESS", "message": "Payment signature verified.",
                        "data": {"order_id": data.get('order_id')}})
    else:
        return jsonify({"status": "ERROR", "message": "Payment signature verification failed.",
                        "data": {"order_id": data.get('order_id')}})


@app.route('/api/customer/<int:customer_id>/passbook', methods=['GET'])
def _get_customer_passbook(customer_id=None):
    _passbook = "SELECT transaction_id, customer_id, amount, current_balance, transaction_type, remarks, created_date FROM t_customer_passbook WHERE customer_id={0}".format(
        customer_id)
    return _get_result(_passbook)


@app.route('/api/bbps/billers', methods=['GET'])
def _get_billers():
    _url = (BBPS_UAT_API_URL + "{}" + "{}").format('biller-list/urn:tenantId:', BBPS_UAT_TENANT_ID)
    _billers = req.post(_url,
                        json={
                            "category": "Fastag",
                        },
                        auth=BBPS_AUTH,
                        headers={
                            "Content-Type": "application/json",
                            "Accept": "application/json"
                        })
    logger.debug("billers: {}".format(_billers))
    return jsonify({"status": "SUCCESS", "data": _billers.json()})


@app.route('/api/bbps/customparams', methods=['GET'])
def _get_customer_parameters():
    _biller_id = request.args.get('billerId')
    if _biller_id:
        _url = (BBPS_UAT_API_URL + "{}" + "{}").format('biller-fetch-custom-params/urn:tenantId:', BBPS_UAT_TENANT_ID)
        _biller_params = req.post(_url,
                                  json={
                                      "billerId": _biller_id
                                  },
                                  auth=BBPS_AUTH,
                                  headers={
                                      "Content-Type": "application/json",
                                      "Accept": "application/json"
                                  })
        logger.debug("Biller params: {}".format(_biller_params))
        return jsonify({"status": "SUCCESS", "data": _biller_params.json()})
    else:
        return jsonify({"status": "FAILURE", "message": "Biller id details is missing."})


@app.route('/api/document/sign', methods=['POST'])
@app.route('/api/document/sign/<document_id>', methods=['GET', 'DELETE'])
@jwt_required
def _get_document_to_sign(document_id=None):
    if request.method == 'POST':
        data = request.get_json()
        dealership_id = data.get("dealership_id")
        loan_id = data.get("loanId")
        _dealers = data.get('dealer')
        _coapplicants = data.get('coapplicants')
        _guarantor = data.get('guarantor', list())
        if data.get('type') == 'sanction':
            document_name = "SanctionLetter"
            template_id = "CqL3GGh"
            payload_template = 'SanctionLetterSigning_payload.json'
            stampSeries = None
        elif data.get('type') == 'agreement':
            document_name = "LoanAgreementLetter"
            template_id = "eNLb5WK"
            payload_template = 'AgreementSigning_payload.json'
            stampSeries = "01"
        elif data.get('type') == 'application':
            document_name = ""
            template_id = ""
            payload_template = ''
            stampSeries = "None"
        return _e_sign_document(
            loan_id, dealership_id,
            {"json_file_name": document_name, "templateId": template_id,
             'Date': datetime.date.today().strftime("%d-%m-%Y")},
            payload_template, stampSeries, data.get('type'), _dealers, _coapplicants, _guarantor
        )
    elif request.method == 'GET':
        return _e_sign_document(document_id=document_id)


def _e_sign_document(loan_id=None, dealership_id=None, file_attributes=None, payload_template=None, stampSeries=None,
                     type=None, _dealers=None, _coapplicants=None, _guarantor=None, document_id=None):
    auth_token = LEEGALITY_AUTH_TOKEN
    if request.method == 'POST':
        invitees_list = list()
        try:
            if not all([_dealers, _coapplicants]):
                return jsonify({"status": "ERROR", "message": error_parser.get('invalid', '_incomplete_details')})

            file_attributes.update({'residence_address': _dealers[0].get('address', ""),
                                    'dealer_first_name': _dealers[0].get('first_name'),
                                    'dealer_last_name': _dealers[0].get('last_name')})
            file_attributes.update(_dict_to_string("coborrower", _coapplicants))
            file_attributes.update(_dict_to_string("guarantor", _guarantor))

            dealership_details = mydb.run_query(mydb.SELECT,
                                                query_parser.get('dealership', '_get_dealership_by_id').format(
                                                    dealership_id))
            file_attributes.update(dealership_details[0])

            loan_data = mydb.run_query(mydb.SELECT,
                                       query_parser.get('dealership', '_get_loan_details_by_id').format(loan_id,
                                                                                                        dealership_id))
            file_attributes.update(loan_data[0])
            if loan_data[0].get("amount_approved") >= 0:
                file_attributes.update({"amount_approved_words": num2words(
                    decimal.Decimal(loan_data[0].get("amount_approved"))) + " Rupees Only"})

            for i in _dealers + _coapplicants + _guarantor:
                invitees_list.append({
                    "name": i.get("first_name") + ' ' + i.get("last_name"),
                    "phone": i.get("mobile", ""),
                    "email": i.get("email", ""),
                    "emailNotification": True,
                    "phoneNotification": True,
                    "webhook": {
                        "success": LEEGALITY_WEB_HOOK,
                        "failure": LEEGALITY_WEB_HOOK,
                        "version": 2.1
                    }
                })
        except Exception as e:
            logger.debug("Exception in document sign because: {}".format(e.message))
            return jsonify({"status": "ERROR", "message": error_parser.get('invalid', '_incomplete_details')})

        template_filepath = os.path.join(os.getcwd(), "template", payload_template)
        target_folder = os.path.join(os.getcwd(), "template")
        target_filepath = os.path.join(os.getcwd(), "template", 'temp.json')

        _url = LEEGALITY_URL + "/api/v2.1/sign/request"
        _headers = {
            'X-Auth-Token': auth_token,
            'Content-Type': 'application/json'
        }
        try:
            if type == "application":
                _file = _generate_application_letter(dealership_id, loan_id).get("file")
                payload = {
                    "file": {
                        "name": "Application Letter",
                        "file": _file,
                    },
                    "invitees": [],
                    "irn": "",
                    "requestSignOrder": False
                }
            else:
                _find_and_replace(template_filepath, target_folder, 'temp.json', file_attributes)
                with open(target_filepath) as template_file:
                    payload = json.loads(template_file.read(), strict=False)
                logger.debug("Closing files")
                template_file.close()
                # logger.debug(sanction_file_data.decode())
                if stampSeries is not None:
                    payload.update({"stampSeries": stampSeries})
                logger.info("Leegality raw invitees: {}".format(invitees_list))
            payload.update({"invitees": invitees_list})
            payload.update({"irn": uuid.uuid1().int})

            try:
                logger.info("Leegality raw payload: {}".format(payload))
                logger.info("Leegality payload: {}".format(json.dumps(payload)))
                response = req.post(_url, headers=_headers, data=json.dumps(
                    payload))  # json.dumps is mandatory to convert single quote json into double quote JSON string
                logger.debug("Leegality response {}:".format(response.text))
                response_json = json.loads(response.text)
                res_document_id = response_json.get("data").get("documentId")
                _query = mydb._gen_insert_query('e_sign_stamp_history', {
                    "dealership_id": dealership_id,
                    "document_id": res_document_id,
                    "document_type": type,
                    "loan_id": loan_id,
                    "invitees_remain": len(invitees_list)
                })
                logger.debug("Leegality response info {}".format(response_json))
                logger.debug("e-sign document_id: {}".format(res_document_id))
                if response_json.get("status"):
                    _result = _execute_query(mydb.INSERT, _query)
                    return jsonify({"status": "SUCCESS", "message": "Document signing process has been initiated",
                                    "data": response_json.get("data")})
                else:
                    return jsonify({"status": "ERROR", "message": response_json.get("messages")[0].get("message"),
                                    "data": response_json.get("data")})

            except req.exceptions.Timeout:
                # Maybe set up for a retry, or continue in a retry loop
                return jsonify({"status": "SUCCESS", "message": error_parser.get('invalid', '_timeout')})
            except req.exceptions.TooManyRedirects:
                # Tell the user their URL was bad and try a different one
                return jsonify({"status": "SUCCESS", "message": error_parser.get('invalid', '_too_many_redirects')})
            except req.exceptions.RequestException:
                # catastrophic error. bail.
                return jsonify({"status": "SUCCESS", "message": error_parser.get('invalid', '_request_exception')})
            except req.exceptions.HTTPError:
                return jsonify({"status": "SUCCESS", "message": error_parser.get('invalid', '_http_error')})

        except IOError as err:
            logger.debug("Workflow file not found in path")
            logger.debug(err)
            return jsonify({"status": "ERROR", "message": "Workflow template file not found in the template path"})

    elif request.method == 'GET':
        if auth_token is not None:
            try:
                _url = LEEGALITY_URL + "/api/v2.1/sign/request"
                _headers = {
                    'X-Auth-Token': auth_token,
                    'Content-Type': 'application/json'
                }
                data = {
                    "document_id": document_id
                }
                response = req.get(_url, headers=_headers, data=data)
                logger.debug("Leegality response {}:".format(response.text))
                return jsonify(
                    {"status": "SUCCESS", "messsage": "e-sign request successful", "data": json.loads(response.text)})

            except req.exceptions.Timeout:
                # Maybe set up for a retry, or continue in a retry loop
                return jsonify({"status": "SUCCESS", "message": error_parser.get('invalid', '_timeout')})
            except req.exceptions.TooManyRedirects:
                # Tell the user their URL was bad and try a different one
                return jsonify({"status": "SUCCESS", "message": error_parser.get('invalid', '_too_many_redirects')})
            except req.exceptions.RequestException:
                # catastrophic error. bail.
                return jsonify({"status": "SUCCESS", "message": error_parser.get('invalid', '_request_exception')})
            except req.exceptions.HTTPError:
                return jsonify({"status": "SUCCESS", "message": error_parser.get('invalid', '_http_error')})

        else:
            return jsonify({"status": "ERROR", "message": "Auth token is empty"})
    elif request.method == 'DELETE':
        if auth_token is not None:
            try:
                _url = LEEGALITY_URL + "/api/v2.1/sign/request"
                _headers = {
                    'X-Auth-Token': auth_token,
                    'Content-Type': 'application/json'
                }
                data = {
                    "document_id": document_id
                }
                response_text = req.delete(_url, headers=_headers, data=data)
                logger.debug("Leegality response {}:".format(response_text.text))
                return jsonify({"status": "SUCCESS", "message": json.loads(response_text.text)})

            except req.exceptions.Timeout:
                # Maybe set up for a retry, or continue in a retry loop
                return jsonify({"status": "SUCCESS", "message": error_parser.get('invalid', '_timeout')})
            except req.exceptions.TooManyRedirects:
                # Tell the user their URL was bad and try a different one
                return jsonify({"status": "SUCCESS", "message": error_parser.get('invalid', '_too_many_redirects')})
            except req.exceptions.RequestException:
                # catastrophic error. bail.
                return jsonify({"status": "SUCCESS", "message": error_parser.get('invalid', '_request_exception')})
            except req.exceptions.HTTPError:
                return jsonify({"status": "SUCCESS", "message": error_parser.get('invalid', '_http_error')})
        else:
            return jsonify({"status": "ERROR", "message": "Auth token is empty"})


@app.route('/api/document/reactivate/<document_id>', methods=['GET'])
@app.route('/api/document/reactivate/<document_id>/<int:expiry_days>', methods=['GET'])
@jwt_required
def _reactivate_document(document_id=None, expiry_days=10):
    if document_id is not None:
        try:
            response = req.post(LEEGALITY_URL + "/api/v3.0/sign/request/reactivate",
                                params={'documentId': document_id, "expiryDays": expiry_days},
                                headers={'X-Auth-Token': LEEGALITY_AUTH_TOKEN})
            logger.debug("Response: ", response.text)
            return jsonify({"status": "SUCCESS", "message": "Sign request activation successful",
                            "data": json.loads(response.text)})
        except req.exceptions.Timeout:
            return jsonify({"status": "ERROR", "message": error_parser.get('invalid', '_timeout')})
        except req.exceptions.TooManyRedirects:
            return jsonify({"status": "ERROR", "message": error_parser.get('invalid', '_too_many_redirects')})
        except req.exceptions.RequestException:
            return jsonify({"status": "ERROR", "message": error_parser.get('invalid', '_request_exception')})
        except req.exceptions.HTTPError:
            return jsonify({"status": "ERROR", "message": error_parser.get('invalid', '_http_error')})
    return jsonify({"status": "ERROR", "message": "No document found. Check if the document id is correct."})


@app.route('/api/document/resend', methods=['POST'])
@jwt_required
def _resend_document():
    data = request.get_json()
    try:
        response = req.post(LEEGALITY_URL + "/api/v3.0/sign/request/resend",
                            params={'signUrls': data.get("sign_url")},
                            headers={'X-Auth-Token': LEEGALITY_AUTH_TOKEN})
        logger.debug("Response: ", response.text)
        return jsonify(
            {"status": "SUCCESS", "message": "Notification sent successfully.", "data": json.loads(response.text)})
    except req.exceptions.Timeout:
        return jsonify({"status": "ERROR", "message": error_parser.get('invalid', '_timeout')})
    except req.exceptions.TooManyRedirects:
        return jsonify({"status": "ERROR", "message": error_parser.get('invalid', '_too_many_redirects')})
    except req.exceptions.RequestException:
        return jsonify({"status": "ERROR", "message": error_parser.get('invalid', '_request_exception')})
    except req.exceptions.HTTPError:
        return jsonify({"status": "ERROR", "message": error_parser.get('invalid', '_http_error')})


@app.route('/api/document/history/<int:loan_id>', methods=['GET'])
@jwt_required
def _get_document_by_loan(loan_id=None):
    if loan_id is not None:
        _query = query_parser.get("e_sign_stamp", "_get_documents_by_loan_id").format(loan_id)
        if request.args.get('document_type'):
            _query = query_parser.get("e_sign_stamp", "_get_documents_by_document_type").format(loan_id,
                                                                                                request.args.get(
                                                                                                    'document_type'))
        _result = mydb.run_query(mydb.SELECT, _query)
        return jsonify({"status": "SUCCESS", "data": _result})


@app.route('/api/document/trail/<document_id>', methods=['GET'])
@jwt_required
def _get_audit_trail(document_id=None):
    if document_id is not None:
        response = req.get(LEEGALITY_URL + "/api/auditTrail",
                           params={'documentId': document_id, 'auditTrail': True},
                           headers={'X-Auth-Token': LEEGALITY_AUTH_TOKEN})
        logger.debug("Response: ", response.json())
        return jsonify({"status": "SUCCESS", "message": "Audit trail data", "data": response.json()})
    return jsonify({"status": "ERROR", "msg": "No document found. Check if the document id is correct."})


@app.route('/api/document/details/<document_id>', methods=['GET'])
@jwt_required
def _get_document_details(document_id=None):
    if document_id is not None:
        try:
            response = req.get(LEEGALITY_URL + "/api/v3.0/document/details",
                               params={'documentId': document_id, 'auditTrail': True},
                               headers={'X-Auth-Token': LEEGALITY_AUTH_TOKEN})
            logger.debug("Response: ", response.json())
            return jsonify({"status": "SUCCESS", "data": response.json()})
        except req.exceptions.Timeout:
            return jsonify({"status": "ERROR", "message": error_parser.get('invalid', '_timeout')})
        except req.exceptions.TooManyRedirects:
            return jsonify({"status": "ERROR", "message": error_parser.get('invalid', '_too_many_redirects')})
        except req.exceptions.RequestException:
            return jsonify({"status": "ERROR", "message": error_parser.get('invalid', '_request_exception')})
        except req.exceptions.HTTPError:
            return jsonify({"status": "ERROR", "message": error_parser.get('invalid', '_http_error')})
    return jsonify({"status": "ERROR", "message": "No document found. Check if the document id is correct."})


@app.route('/api/leegality/report', methods=['POST'])
def _get_webhook():
    try:
        data = request.get_json()
        with open('template/pdf_test.json', 'w') as outfile:
            json.dump(data, outfile)
        e_sign_data = mydb.run_query(mydb.SELECT,
                                     query_parser.get('e_sign_stamp', '_get_sign_details_by_document_id').format(
                                         data.get('documentId')))
        logger.debug("Input report from Leegality for irn: {}".format(data.get('irn')))
        if len(e_sign_data) == 0 or not data.get('files'):
            return jsonify({"status": "ERROR", "message": "Provided document_id doesnt exist"})
        _target_file_path = os.path.join(os.getcwd(), "template", 'temp_e_sign_stamp.pdf')
        bytes = base64.b64decode(data.get('files')[0], validate=True)
        if bytes[0:4] != b'%PDF':
            raise ValueError('Missing the PDF file signature')
        try:
            logger.debug("base64 file payload is being written as PDF.")
            f = open(_target_file_path, 'wb')
            f.write(bytes)
            f.close()
        except Exception as e:
            logger.debug("Error in writing the file as PDF because: {}".format(e.message))
            return jsonify({"status": "ERROR", "message": "Internal file error."})

        dealership_id = e_sign_data[0].get('dealership_id')
        _type = e_sign_data[0].get('document_type')
        loan_id = e_sign_data[0].get('loan_id')
        with open(_target_file_path, 'rb') as new_file:
            logger.debug("File type during upload: {}".format(type(new_file)))
            _upload_status = _s3_file_upload(str(dealership_id) + "/{0}/{1}/{2}.pdf".format(loan_id, _type, _type),
                                             dealership_docs_bucket, new_file, "application/pdf")
            if _upload_status:
                _url = docs_url + "/" + str(dealership_id) + "/{0}/{1}/{2}.pdf".format(loan_id, _type,
                                                                                                      _type)

                logger.debug("Uploaded document to path %s", _url)
                result = mydb.run_query(mydb.UPDATE,
                                        "UPDATE e_sign_stamp_history SET document_url='{0}' WHERE id={1}".format(
                                            _url, e_sign_data[0].get('id')))
                logger.debug("Document URL update result: {}".format(result))

            else:
                logger.debug("Upload Error: Document could not be uploaded to S3. Please try again.")
                return jsonify({"status": "ERROR",
                                "message": "Document generated successfully but could not be uloaded to S3. "
                                           "Please contact administrator."})

        _query = mydb._gen_update_query('e_sign_stamp_history', ["mac", "signer", "request"],
                                        data) + ",invitees_remain=invitees_remain-1 WHERE {0}='{1}'".format(
            "document_id", data.get("documentId"))
        return _execute_query(mydb.UPDATE, _query)
    except:
        return jsonify({"status": "ERROR", "message": "Invalid input"})


@app.route('/api/document/completed', methods=['GET'])
@jwt_required
def _get_document_completed():
    try:
        _url = LEEGALITY_URL + "/api/v3.0/document/completed"
        _headers = {
            'X-Auth-Token': os.getenv('AUTH_TOKEN'),
            'Content-Type': 'application/json'
        }
        response = req.get(_url, headers=_headers)
        return jsonify({"status": "SUCCESS", "message": json.loads(response.text)})

    except req.exceptions.Timeout:
        return jsonify({"status": "ERROR", "message": error_parser.get('invalid', '_timeout')})
    except req.exceptions.TooManyRedirects:
        return jsonify({"status": "ERROR", "message": error_parser.get('invalid', '_too_many_redirects')})
    except req.exceptions.RequestException:
        return jsonify({"status": "ERROR", "message": error_parser.get('invalid', '_request_exception')})
    except req.exceptions.HTTPError:
        return jsonify({"status": "ERROR", "message": error_parser.get('invalid', '_http_error')})


@app.route('/api/loans/dealership/<int:dealership_id>/loans/<int:loan_id>/sanction', methods=['GET'])
@jwt_required
def _generate_sanction_letter_base64(dealership_id=None, loan_id=None):
    logger.debug("Generating sanction letter for approved loan, id:{}".format(str(loan_id)))
    _cur_date = datetime.date.today()
    query = query_parser.get('dealership', '_loan_sanction_letter').format(dealership_id)
    data = mydb.run_query(mydb.SELECT, query)
    if data:
        logger.debug("Data in request ----> %s", data[0])
        data[0].update({"date": "{}-{}-{}".format(_cur_date.day, _cur_date.month, _cur_date.year)})
        data = data[0]
        logger.debug("Sanction letter data: %s", data)
        _sanction_letter_template = app.config['SANCTION_LETTER_TEMPLATE']
        _target_file_path = app.config['TARGET_FILE_PATH'] + "/{}/".format(str(dealership_id))
        _file_name = "{}_sanction_letter.html".format(dealership_id)
        _sanction_letter = _find_and_replace(_sanction_letter_template, _target_file_path, _file_name, data)
        logger.debug("Replaced file %s", _sanction_letter)
        options.update({'header-html': 'template/header.html'})
        with open(_sanction_letter) as fin:
            logger.debug("Generating sanction letter...")
            pdfkit.from_file(fin, os.path.join(_target_file_path, "sanction_letter.pdf"), options=options,
                             configuration=_wkhtml_config)
        logger.debug(os.path.join(_target_file_path, "sanction_letter.pdf"))
        message_bytes = open(os.path.join(_target_file_path, "sanction_letter.pdf"), "rb").read()
        file_bytes = base64.b64encode(message_bytes)
        file_message = file_bytes.decode('ascii')

        return jsonify(
            {"status": "SUCCESS", "message": "base64 Encoded PDF", "file": file_message})

    else:
        return jsonify(
            {"status": "ERROR", "message": "Sanction letter could not be generated. Please contact administrator."})


@app.route('/api/loans/dealership/<int:dealership_id>/loans/<int:loan_id>/application', methods=['GET'])
@jwt_required
def _generate_application_letter(dealership_id=None, loan_id=None):
    logger.debug("Generating application letter for approved loan, id:{}".format(str(loan_id)))
    _cur_date = datetime.date.today()
    query = query_parser.get('dealership', '_loan_sanction_letter').format(dealership_id)
    data = mydb.run_query(mydb.SELECT, query)
    _dealers = mydb.run_query(mydb.SELECT, query_parser.get("dealers", "_get_associated_dealers").format(dealership_id))
    _coapplicants = mydb.run_query(mydb.SELECT, query_parser.get("coapplicants", "_get_associated_coapplicants").format(
        dealership_id))
    if data:
        logger.debug("Data in request ----> %s", data[0])
        data[0].update({"date": "{}-{}-{}".format(_cur_date.day, _cur_date.month, _cur_date.year)})
        data = data[0]
        dealer_details = list(_coapplicants)
        dealer_details.append(_dealers[0])
        data.update({"_dealers": _list_to_string("_dealers", _dealers)})
        data.update({"_coapplicants": _list_to_string("_coapplicants", _coapplicants)})
        data.update({"dealer_details": _list_to_string("dealer_details", dealer_details)})
        logger.debug("Application letter data: %s", data)
        _application_template = app.config['APPLICATION_TEMPLATE']

        _target_file_path = app.config['PROJECT_DIR'] + "/data/document/{}/".format(str(dealership_id))
        _file_name = "{}_application.html".format(dealership_id)
        _pdf_name = "{}_application.pdf".format(dealership_id)
        _application = _find_and_replace(_application_template, _target_file_path, _file_name, data)
        logger.debug("Replaced file %s", _application)
        options.update({'header-html': 'template/header.html'})
        with open(_application) as fin:
            logger.debug("Generating application letter...")
            pdfkit.from_file(fin, os.path.join(_target_file_path, _pdf_name), options=options,
                             configuration=_wkhtml_config)
        message_bytes = open(os.path.join(_target_file_path, _pdf_name), "rb").read()

        file_bytes = base64.b64encode(message_bytes)
        file_message = file_bytes.decode('ascii')

        return ({"status": "SUCCESS", "message": "base64 Encoded PDF", "file": file_message})
    else:
        return jsonify(
            {"status": "ERROR", "message": "Application letter could not be generated. Please contact administrator."})


def _list_to_string(_template_name=None, _data=None, _res=""):
    _file = open(app.config['TEMPLATE_DIR'] + "/" + _template_name + '.html', "r")  # get template from key_value k
    _file_temp = _file.read()
    _file.close()
    for _val in range(len(_data)):
        _template = _file_temp
        for _k, _v in _data[_val].items():
            _template = _template.replace("#" + _k + "#", str(_v))
        _res = _res + _template
    return _res


# def _hangup_url('/api/loans/reminder")


@app.route('/api/send/mail/<int:dealership_id>', methods=['GET'])
@app.route('/api/send/mail', methods=['POST'])
def _send_mail(mail_data, attachment=None, file_name=None, file_type="application/pdf"):
    try:
        mail = Mail()
        app.config.update(mail_config)
        mail.init_app(app)
        message = _get_email_message_context(mail_data.get('subject'), recipients=mail_data.get('recipients'),
                                             body=mail_data.get('body'))
        if attachment is not None:
            for sub_attachment, name in itertools.zip_longest(attachment, file_name):
                with app.open_resource(sub_attachment) as fp:
                    logger.debug("Email attachment is in path: {}".format(sub_attachment))
                    message.attach(name, file_type, fp.read())
                    logger.debug("File '{}' is attached for email.".format(name))
        logger.debug("Email send request initiated.....")
        mail.send(message)
        logger.debug("Email sent successfully.")
        return jsonify({"message": "Email sent successfully.", "delivery": True})
    except Exception as e:
        logger.debug("Email error---->: {}".format(e))
        return jsonify({"message": "Email could not be sent, contact administrator.", "delivery": False})


def _get_email_message_context(subject="Test email subject", sender=None, recipients=None, body="Test body content",
                               attachments=""):
    if sender is None:
        sender = app.config.get('MAIL_USERNAME')
    msg = Message(subject=subject,
                  sender=sender,
                  recipients=recipients,
                  body=body,
                  attachments=attachments)
    return msg


def _file_upload_local(file, dealership_id=None, operation='INSERT'):
    dir_path = os.getcwd()
    folder_path = ""
    file_url = ""
    if (dealership_id is not None):
        folder_path = dir_path + "/" + app.config['UPLOAD_FOLDER'] + "/" + str(dealership_id)
        file_url = app.config['UPLOAD_FOLDER'] + "/" + str(dealership_id)
    else:
        logger.debug("Dealership details are missing in folder path.")
        return jsonify({"status": "ERROR", "message": "File could not be uploaded. Contact IT Head."})

    if operation == 'INSERT':

        if file.filename == '':
            return jsonify({"status": "ERROR", "message": "Select a file to upload."})

        if file and allowed_file(file.filename):
            filename = secure_filename(file.filename)
            if (os.path.exists(folder_path) == False):
                logger.debug("Creating the data directory: {}".format(folder_path))
                os.makedirs(folder_path)
            file.save(os.path.join(folder_path, filename))
            return ("https://api.petromoney.in/" + file_url + "/" + filename)
        else:
            return jsonify(
                {"status": "ERROR",
                 "message": "Only files with 'pdf', 'png', 'jpg', 'jpeg', 'xls', 'xlsx', 'csv' extensions are allowed. "})

    if operation == 'DELETE':
        filename = file.rsplit('/', 1)[-1]
        try:
            if os.path.isfile(os.path.join(folder_path, filename)):
                logger.debug("Deleting file --> {} from path {}".format(filename, folder_path))
                os.remove(os.path.join(folder_path, filename))
                return True
        except OSError as e:
            logger.debug("File OS Error: {}".format(e.message))
            return False


def _file_upload(files, dealership_id=None, dealer_id=None, doc_id=None):
    if dealership_id is not None:
        _file_urls = list()
        _query_result = mydb.run_query(mydb.SELECT, query_parser.get('checklist', '_get_document_name').format(doc_id))
        doc_name = _query_result[0].get('doc_name')
        _s3_file_prefix = str(dealership_id) + "/" + doc_name
        if dealer_id is not None:
            _s3_file_prefix = _s3_file_prefix + "/" + str(dealer_id) + "/" + doc_name
        for file in files:
            if file and allowed_file(file.filename):
                filename = secure_filename(file.filename)
                logger.info("Filename to be uploaded... {}".format(filename))
                _upload_status = _s3_file_upload(_s3_file_prefix + "/" + filename,
                                                 dealership_docs_bucket, file)
                if _upload_status:
                    _file_urls.append(docs_url + "/" + _s3_file_prefix + "/" + filename)
                else:
                    return jsonify(
                        {"status": "ERROR", "message": "File could not be uploaded to S3. Please try again."})
            else:
                return jsonify(
                    {"status": "ERROR",
                     "message": "No file is selected for upload or the file has an extension other than '.pdf', '.png', "
                                "'.jpg', '.jpeg', '.xls', '.xlsx', '.csv'"})
        logger.info("File URLs.... {}".format(_file_urls))
        return True, " ".join(str(x) for x in _file_urls)
    else:
        logger.debug("Dealership details are missing in function call.")
        return jsonify({"status": "ERROR", "message": "File could not be uploaded. Please contact administrator"})


def _transporter_file_upload(files, transport_owner_id=None, transporter_id=None, doc_id=None):
    if doc_id and (transport_owner_id or transporter_id):
        logger.debug("transport_owner_id {}".format(transport_owner_id))
        logger.debug("transporter_id {}".format(transporter_id))
        _file_urls = list()
        _query_result = mydb.run_query(mydb.SELECT, query_parser.get('checklist', '_get_document_name').format(doc_id))
        doc_name = _query_result[0].get('doc_name')
        if transport_owner_id is None:
            _s3_file_prefix = str(transporter_id) + "/" + doc_name
        elif transporter_id is None:
            _s3_file_prefix = str(transport_owner_id) + "/" + doc_name
        else:
            logger.debug("transport_owner_id {}".format(transport_owner_id))
            logger.debug("transporter_id {}".format(transporter_id))
            _s3_file_prefix = str(transport_owner_id) + "/" + str(transporter_id) + "/" + doc_name
        for file in files:
            if file and allowed_file(file.filename):
                filename = secure_filename(file.filename)
                logger.info("Filename to be uploaded... {}".format(filename))
                _upload_status = _s3_file_upload("transporters/" + _s3_file_prefix + "/" + filename,
                                                 transporter_docs_bucket, file)
                if _upload_status:
                    _file_urls.append(docs_url + "/transporters/" + _s3_file_prefix + "/" + filename)
                else:
                    return jsonify(
                        {"status": "ERROR", "message": "File could not be uploaded to S3. Please try again."})
            else:
                return jsonify(
                    {"status": "ERROR",
                     "message": "No file is selected for upload or the file has an extension other than '.pdf', '.png', "
                                "'.jpg', '.jpeg', '.xls', '.xlsx', '.csv'"})
        logger.info("File URLs.... {}".format(_file_urls))
        return True, " ".join(str(x) for x in _file_urls)
    else:
        logger.debug("Transporter document details are missing in function call.")
        return jsonify({"status": "ERROR", "message": "File could not be uploaded. Please contact administrator"})


def _vehicle_file_upload(files, transporter_id=None, vehicle_id=None, document_name=None, doc_id=None):
    if transporter_id is not None and vehicle_id is not None:
        _file_urls = list()
        _file_urls = list()
        _query_result = mydb.run_query(mydb.SELECT, query_parser.get('checklist', '_get_document_name').format(doc_id))
        doc_name = _query_result[0].get('doc_name')
        _s3_file_prefix = str(transporter_id) + "/" + str(vehicle_id) + "/" + doc_name
        if document_name is not None:
            _s3_file_prefix = str(transporter_id) + "/" + str(vehicle_id) + "/" + doc_name + "_" + document_name
        for file in files:
            if file and allowed_file(file.filename):
                filename = secure_filename(file.filename)
                logger.info("Filename to be uploaded... {}".format(filename))
                _upload_status = _s3_file_upload("transporters/" + _s3_file_prefix + "/" + filename,
                                                 transporter_docs_bucket, file)
                if _upload_status:
                    _file_urls.append(docs_url + "/transporters/" + _s3_file_prefix + "/" + filename)
                else:
                    return jsonify(
                        {"status": "ERROR", "message": "File could not be uploaded to S3. Please try again."})
            else:
                return jsonify(
                    {"status": "ERROR",
                     "message": "No file is selected for upload or the file has an extension other than '.pdf', '.png', "
                                "'.jpg', '.jpeg', '.xls', '.xlsx', '.csv'"})
        logger.info("File URLs.... {}".format(_file_urls))
        return True, " ".join(str(x) for x in _file_urls)
    else:
        logger.debug("Vehicle document details are missing in function call.")
        return jsonify({"status": "ERROR", "message": "File could not be uploaded. Please contact administrator"})


def _s3_file_upload(_file_to_upload, _bucket_name, _file_object, _content_type=None):
    _upload_status = False
    client = boto3.client('s3')
    logger.debug("A file with key %s is being uploaded to bucket %s", _file_to_upload, _bucket_name)
    try:
        if _content_type is not None:
            _upload_response = client.put_object(Body=_file_object.read(), Bucket=_bucket_name, Key=_file_to_upload,
                                                 ContentType=_content_type)
        else:
            _upload_response = client.put_object(Body=_file_object.read(), Bucket=_bucket_name, Key=_file_to_upload)
        logger.debug("File upload to S3 response: {}".format(_upload_response))
        if _upload_response:
            _upload_status = True
    except Exception as e:
        logger.debug("File upload to s3 failed because: {}", format(e))
    return _upload_status


def _is_rows_affected(query, _update_status=False, stage_id=0, dealership_id=0):
    logger.debug("Query to be executed: {}".format(query))
    affected_rows = mydb.run_query(mydb.INSERT, query)
    if (affected_rows > 0) or _update_status:
        profile_status = _check_profile_status_and_upsert(_update_status, stage_id, dealership_id)
        logger.debug("Status updated into the stage table.")
        return jsonify({'status': 'SUCCESS',
                        'message': 'Updated recorded successfully.',
                        'affected_rows': affected_rows,
                        'profile_status': profile_status})
    else:
        return jsonify({'status': 'ERROR', 'message': 'Failed to add the record.'})


def _execute_query_with_status(query_type, query, _update_status=False, stage_id=0, dealership_id=0):
    logger.debug("_is_rows_affected - Query ---> {}".format(query))
    affected_rows, last_insert_id = mydb.run_query(query_type, query, row_insert_id=True)
    logger.debug("Affected rows: {} and last insert row id: {}".format(str(affected_rows), str(last_insert_id)))
    if (affected_rows > 0):
        profile_status = _check_profile_status_and_upsert(_update_status, stage_id, dealership_id)
        return jsonify({'status': 'SUCCESS', 'affected_rows': affected_rows, 'row_id': last_insert_id,
                        'profile_status': profile_status})
    else:
        return jsonify({'status': 'ERROR', 'message': 'Failed to add the record.'})


def _execute_query(query_type, query):
    logger.debug("_is_rows_affected - Query ---> {}".format(query))
    affected_rows, last_insert_id = mydb.run_query(query_type, query, row_insert_id=True)
    logger.debug("Affected rows: {} and last insert row id: {}".format(affected_rows, last_insert_id))
    if affected_rows > 0:
        return jsonify(
            {'status': 'SUCCESS', 'message': 'Record add/update was successful.', 'affected_rows': affected_rows,
             'row_id': last_insert_id})
    elif affected_rows == 0:
        return jsonify({'status': 'ERROR', 'message': 'No record was updated.', 'affected_rows': affected_rows,
                        'row_id': last_insert_id})
    else:
        return jsonify({'status': 'ERROR', 'message': 'Failed to add the record.'})


def _check_profile_completion_status(dealership_id=None):
    _status_check_for_list = [1, 2, 3]
    _status_list = [e['stage_id'] for e in (
        _get_result(query_parser.get('dealership', '_get_profile_status_list').format(dealership_id))).get_json().get(
        'data')]
    status = all(elem in _status_list for elem in _status_check_for_list)
    incomplete_stage = [i for i in _status_check_for_list if i not in _status_list]
    return jsonify(
        {"completion_status": status, "stages_complete": _status_list, "stages_incomplete": incomplete_stage})


def _check_profile_status_and_upsert(_update_status=False, stage_id=0, dealership_id=0):
    if (_update_status == True and stage_id > 0 and dealership_id > 0):
        _is_exists = mydb.run_query(mydb.SELECT,
                                    query_parser.get('dealership', '_if_status_exists').format(stage_id, dealership_id))
        if (len(_is_exists) < 1):
            mydb.run_query(mydb.INSERT,
                           query_parser.get('dealership', '_post_profile_completion_status').format(dealership_id,
                                                                                                    stage_id))
            return "status updated"
        else:
            return "Profile exists/updated, no status update required."
    else:
        return "status updated already"


def _get_result(query):
    result = mydb.run_query(mydb.SELECT, query)
    return jsonify({'status': 'SUCCESS', 'data': result})


def _get_result_as_dict(query):
    return mydb.run_query(mydb.SELECT, query)


def _is_empty(val):
    return False if val else True


def _format_files_data(data, key):
    sorted_data = sorted(data, key=itemgetter(key))
    file_data = list()
    for key, value in groupby(sorted_data, key=itemgetter(key)):
        files = list()
        for i in value:
            files.append({'file_id': i.get('id'), 'file_url': i.get('file_path'),
                          'file_type': _get_file_type(i.get('file_path'))})
        i.pop("id")
        i.pop("file_path")
        i.update({"file_data": files})
        file_data.append(i)
    return file_data


def _get_data_from_lms(query):
    cnxn = _get_lms_connection()
    cursor = cnxn.cursor()
    logger.debug("Query to Oracle DB LMS: %s", query)
    cursor.execute(query)
    column_names_list = [x[0].lower() for x in cursor.description]
    result = [dict(zip(column_names_list, row)) for row in cursor.fetchall()]
    cnxn.close()
    return result


def _execute_lms_query(query):
    cnxn = _get_lms_connection()
    cursor = cnxn.cursor()
    logger.debug("Query to Oracle DB LMS: {}".format(query))
    result = cursor.execute(query)
    logger.debug("Oracle execute query result: {}".format(result))
    cnxn.commit()
    cnxn.close()
    return result


def _get_lms_connection():
    return cx_Oracle.connect('QC_MASTER/PeQu2O21@prodpetro.cwalwexsicx2.ap-south-1.rds.amazonaws.com/orcl')

def _hash_password(password):
    # """Hash a password for storing."""
    salt = hashlib.sha256(os.urandom(60)).hexdigest().encode('ascii')
    pwdhash = hashlib.pbkdf2_hmac('sha512', password.encode('utf-8'), salt, 100000)
    pwdhash = binascii.hexlify(pwdhash)
    return (salt + pwdhash).decode('ascii')


def _verify_password(stored_password, provided_password):
    # """Verify a stored password against one provided by user"""
    salt = stored_password[:64]
    stored_password = stored_password[64:]
    pwdhash = hashlib.pbkdf2_hmac('sha512', provided_password.encode('utf-8'), salt.encode('ascii'), 100000)
    pwdhash = binascii.hexlify(pwdhash).decode('ascii')
    return pwdhash == stored_password


@app.route('/api/otp/send', methods=['POST'])
def send_otp(mobile, template_id=None, otp_expiry=1):
    response = req.post('https://api.msg91.com/api/v5/otp', params={
        'invisible': '1',
        'authkey': SMS_API_AUTH_KEY,
        'mobile': '+91' + str(mobile),
        'template_id': template_id,
        'otp_expiry': otp_expiry
    })
    logger.debug("Response: {}".format(response.json()))
    return response.json()


@app.route('/api/otp/resend', methods=['POST'])
def resend_otp():
    data = request.get_json()
    logger.debug(data)
    mobile = data.get('mobile')
    retry_type = data.get('retry_type', 'text')
    response = req.post('https://api.msg91.com/api/v5/otp/retry', params={
        'mobile': '+91' + mobile,
        'authkey': SMS_API_AUTH_KEY,
        'retrytype': retry_type
    })
    return jsonify({"status": "SUCCESS", "message": response.json().get('message')})


def _create_pay_order(customer_id, data):
    order_amount = int(data.get('amount', 0)) * 100
    order_currency = data.get('currency', 'INR')
    order_receipt = data.get('receipt', 'pm_rcp_' + str(customer_id))
    notes = data.get('remarks', 'NA')
    _order_response = dict()
    result = _rpay_client.order.create(
        dict(amount=order_amount, currency=order_currency, receipt=order_receipt, notes=dict(remarks=notes)))
    _order_response.update({'customer_id': customer_id,
                            'rzp_order_id': result.get('id'),
                            'amount': int(result.get('amount') / 100),
                            'receipt': result.get('receipt'),
                            'status': result.get('status'),
                            'transaction_ts': result.get('created_at'),
                            'payment_type_id': 1,
                            'remarks': json.dumps(result.get('notes'))
                            })
    logger.debug("RZP response type: {0} \nand the result is: {1}".format(type(result), result))
    _query = mydb._gen_insert_query('t_razorpay_transaction_log', _order_response)
    _log_result = _execute_query(mydb.INSERT, _query)
    logger.debug("RZP transaction log result: {0}".format(_log_result))
    return _order_response


def _insert_customer_passbook(customer_id, data):
    _passbook_data = {
        "customer_id": customer_id,
        "amount": data.get('amount', 0),
        "remarks": data.get('remarks', ""),
        "transaction_type": data.get('transaction_type', 1)
    }
    if _passbook_data.get('transaction_type') == 0:
        _passbook_data.update({"current_balance": (_get_current_balance(customer_id) - data.get('amount'))})
    else:
        _passbook_data.update({"current_balance": (_get_current_balance(customer_id) + int(data.get('amount')))})
    _trans_query = mydb._gen_insert_query('t_customer_passbook', _passbook_data)
    _passbook_entry_result = _execute_query(mydb.INSERT, _trans_query)
    logger.debug("Customer passbook entry result: {0}".format(_passbook_entry_result))
    return True


@app.route('/api/dealership/<int:dealership_id>/order/<order_id>/payment/capture', methods=['POST'])
def _capture_payment_signature():
    return 0


@app.route('/api/customer/<int:customer_id>/balance', methods=["GET"])
def _customer_balance(customer_id=None):
    return jsonify({"status": "SUCCESS", "data": {"current_balance": _get_current_balance(customer_id)}})


def _get_current_balance(customer_id):
    _balance = mydb.run_query(mydb.SELECT,
                              "SELECT current_balance FROM t_customer_passbook WHERE customer_id={} ORDER BY created_date DESC LIMIT 1".format(
                                  customer_id))
    logger.debug("Account balance details are: {}".format(_balance))
    if _balance:
        return int(_balance[0].get('current_balance')) if _balance[0].get('current_balance') else 0
    else:
        return 0


def verify_otp(mobile, otp):
    response = req.post('https://api.msg91.com/api/v5/otp/verify', params={
        'mobile': '+91' + str(mobile),
        'authkey': SMS_API_AUTH_KEY,
        'otp': otp
    })
    logger.debug("Verifying the OTP response: %s", response.json())
    return response.json()


def _get_user_regions(user_id):
    return _get_result_as_dict(query_parser.get('users', '_get_user_region_info').format(user_id))


def _user_exists(user_id):
    _user_exists = _get_result_as_dict(
        "SELECT EXISTS(SELECT mobile FROM `m_users` where id={0}) as user_exists".format(user_id))
    return _user_exists[0].get('user_exists', 0)


def _is_main_applicant(mobile_no, dealership_id):
    _user_exists = _get_result_as_dict(
        "SELECT EXISTS(" + query_parser.get('dealers', '_get_main_dealer').format(dealership_id) + " AND mobile={}) as user_exists".format(mobile_no))
    return _user_exists[0].get('user_exists', 0)


def _dealer_exists(mobile_no):
    _dealer_exists = _get_result_as_dict(
        "SELECT EXISTS(SELECT id from m_dealers where mobile={}) as _dealer_exists".format(mobile_no))
    return _dealer_exists[0].get('_dealer_exists', 0)


def _transporter_exists(transporter_id):
    _transporter_exists = _get_result_as_dict(
        "SELECT EXISTS(SELECT transporter_id from `t_transports` where transporter_id={0}) as transporter_exists".format(
            transporter_id))
    return _transporter_exists[0].get('transporter_exists', 0)


def _dealership_loan_exists(dealership_id):
    _loan_exists = _get_result_as_dict(query_parser.get('dealership', '_loan_exists').format(dealership_id))
    return _loan_exists[0].get('loan_exists', 0)


def num2words(num):
    num = decimal.Decimal(num)
    decimal_part = num - int(num)
    num = int(num)
    if decimal_part:
        return num2words(num) + " point " + (" ".join(num2words(i) for i in str(decimal_part)[2:]))
    under_20 = ['Zero', 'One', 'Two', 'Three', 'Four', 'Five', 'Six', 'Seven', 'Eight', 'Nine', 'Ten', 'Eleven',
                'Twelve', 'Thirteen', 'Fourteen', 'Fifteen', 'Sixteen', 'Seventeen', 'Eighteen', 'Nineteen']
    tens = ['Twenty', 'Thirty', 'Forty', 'Fifty', 'Sixty', 'Seventy', 'Eighty', 'Ninety']
    above_100 = {100: 'Hundred', 1000: 'Thousand', 100000: 'Lakhs', 10000000: 'Crores'}
    if num < 20:
        return under_20[num]
    if num < 100:
        return tens[num // 10 - 2] + ('' if num % 10 == 0 else ' ' + under_20[num % 10])
    pivot = max([key for key in above_100.keys() if key <= num])
    return num2words(num // pivot) + ' ' + above_100[pivot] + ('' if num % pivot == 0 else ' ' + num2words(num % pivot))


def _dict_to_string(type=None, _applicant=None):
    name = phone = email = address = ""
    for i in _applicant:
        name = name + i.get("first_name") + ' ' + i.get("last_name") + ','
        phone = phone + str(i.get("mobile", "-")) + ','
        email = email + i.get("email", "-") + ','
        address = address + i.get("address", "-") + ' / '
    return ({
        type + "_names": name[slice(0, -1)],
        type + "_number": phone[slice(0, -1)],
        type + "_mail": email[slice(0, -1)],
        type + "_address": address[slice(0, -1)],  # slice to remove the last unwanted symbol
    })


def _find_and_replace(_template_file=None, _target_file=None, _file_name=None, _key_values=None):
    logger.debug("Template file: %s", _template_file)
    logger.debug("Target file: %s/%s", _target_file, _file_name)
    _template = open(_template_file, "r")
    if not os.path.exists(_target_file):
        os.makedirs(_target_file)
    with open(os.path.join(_target_file, _file_name), "w") as _file:
        for line in _template:
            for k, v in _key_values.items():
                line = line.replace("#" + k + "#", str(v))
            _file.write(line)
    _template.close()
    _file.close()
    return os.path.join(_target_file, _file_name)


def allowed_file(filename):
    if filename == '':
        return jsonify({"status": "ERROR", "message": "Select a file to upload."})
    else:
        return '.' in filename and filename.rsplit('.', 1)[1].lower() in ALLOWED_EXTENSIONS


def _if_none_and_sum(result, key):
    sum = dict(reduce(add, map(Counter, result))).get(key)
    return sum if sum is not None else 0


def _get_file_type(filename):
    if filename is not None:
        return '.' in filename and filename.rsplit('.', 1)[1].lower()
    else:
        return "null"


@app.route('/api/system/alerts', methods=['POST'])
@jwt_required
def alerts():
    data = request.get_json()
    msg = ({'recipients':alert_recipients,
            'subject':data.get('message'),
            'body':data.get('trace')
            })
    return _send_mail(msg)


@app.route('/*', methods=['POST'])
def test():
    logger.info("Reauest header")
    logger.info(request.header)


# Run Server
if __name__ == '__main__':
    logger.debug('Starting the application....')
    app.run(host="0.0.0.0", port="5000", debug="True")
