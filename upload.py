import os
from app import app
from flask import jsonify, request
from werkzeug.utils import secure_filename
import logging as logger

app.config['UPLOAD_FOLDER'] = "data"
app.config['MAX_CONTENT_LENGTH'] = 10 * 1024 * 1024

ALLOWED_EXTENSIONS = {'pdf', 'png', 'jpg', 'jpeg'}


def allowed_file(filename):
    return '.' in filename and filename.rsplit('.', 1)[1].lower() in ALLOWED_EXTENSIONS


@app.route('/api/upload/<int:dealership_id>/<loan_type>', methods=['POST'])
@app.route('/api/upload/<int:dealership_id>/<int:dealer_id>', methods=['POST'])
def upload_file(dealership_id=None, dealer_id=None, loan_type=None):
    if request.method == 'POST':
        if 'file' not in request.files:
            print("No file part")
            return jsonify({"Status": "Failed", "msg": "No File Part"})
        file = request.files['file']

        if file.filename == '':
            print("No file selected for uploading")
            return jsonify({"Status": "Failed", "msg": "No file selected for uploading"})

        dir_path = os.getcwd()
        folder_path = ""
        if all(value is not None for value in [dealership_id, loan_type]):
            folder_path = dir_path + "/" + app.config['UPLOAD_FOLDER'] + "/" + str(dealership_id) + "/" + loan_type
        elif (value is not None for value in [dealership_id, dealer_id]):
            folder_path = dir_path + "/" + app.config['UPLOAD_FOLDER'] + "/" + str(dealership_id) + "/" + str(dealer_id)
        else:
            logger.debug("Incorrect file path.")
            return jsonify({"Status": "Failed", "msg": "Incorrect file path."})

        if file and allowed_file(file.filename):
            filename = secure_filename(file.filename)
            try:
                if(os.path.exists(folder_path) == False):
                    logger.debug("Creating the data directory: ",folder_path)
                    os.makedirs(folder_path)
                    file.save(os.path.join(folder_path, filename))
                    return jsonify({"Status": "Success", "msg": "File successfully uploaded"})
            except OSError as e:
                logger.debug('Error:',e)
                return jsonify({"Status": "Success",
                                "msg": "File could not be uploaded. Please email the document to sales@petromoney.co.in"})
        else:
            print("Allowed file types are pdf, png, jpg, jpeg")
            return jsonify(
                {"Status": "Failed", "msg": "Only files with 'pdf', 'png', 'jpg', 'jpeg' extensions are allowed. "})


if __name__ == "__main__":
    logger.debug('Starting the application', )
    app.run(host="0.0.0.0", port="5100", debug="True")